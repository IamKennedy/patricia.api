﻿using Autofac;
using Patricia.Library.Data;
using Patricia.Library.DataContracts;
using Patricia.Library.Business;
using Patricia.Library.BusinessContracts;
using Patricia.Library.Common;
using Patricia.Library.CommonContracts;
using Patricia.Library.Models;

namespace Patrica.Library.Utilities
{
    public class AutoFacRegistrar
    {
        public static void RegisterTypes(ref ContainerBuilder builder)
        {
            builder.RegisterType<ErrorLogger>().As<IErrorLogger>();

            builder.RegisterType<CardBusinessRepository>().As<ICardBusinessRepository>();
            builder.RegisterType<CardDataRepository>().As<ICardDataRepository>();

            //Re-Implement Transaction Business Repository
            builder.RegisterType<TransactionBusinessRepository>().As<ITransactionBusinessRepository>();
            builder.RegisterType<TransactionDataRepository>().As<ITransactionDataRepository>();

            //Re-Implement User Business Repository
            builder.RegisterType<UserBusinessRepository>().As<IUserBusinessRepository>();
            builder.RegisterType<UserDataRepository>().As<IUserDataRepository>();

            builder.RegisterType<ApprovalConfigDataRepository>().As<IApprovalConfigDataRepository>();

            builder.RegisterType<ApprovalDataRepository>().As<IApprovalDataRepository>();
            builder.RegisterType<ApprovalBusinessRepository>().As<IApprovalBusinessRepository>();

            builder.RegisterType<AuditTrailDataRepository>().As<IAuditTrailDataRepository>();
            builder.RegisterType<AuditTrailBusinessRepository>().As<IAuditTrailBusinessRespository>();

            builder.RegisterType<BinDataRepository>().As<IBinDataRepository>();
            builder.RegisterType<BinBusinessRepository>().As<IBinBusinessRepository>();

            builder.RegisterType<BranchDataRepository>().As<IBranchDataRepository>();
            builder.RegisterType<BranchBusinessRepository>().As<IBranchBusinessRepository>();

            builder.RegisterType<CardPRDataRepository>().As<ICardPRDataRepository>();
            builder.RegisterType<CardPRBusinessRepository>().As<ICardPRBusinessRepository>();

            

            builder.RegisterType<IPDataRepository>().As<IIPDataRepository>();
            builder.RegisterType<IPBusinessRepository>().As<IIPBusinessRepository>();

          

            builder.RegisterType<PatriciaCardEntities>();
            builder.RegisterType<CryptoLib>().As<ICryptoLib>();
        }
    }
}