﻿using Autofac;
using Autofac.Integration.WebApi;
using Patrica.Library.Utilities;
using System.Reflection;
using System.Web.Http;

namespace Patricia.Library.Utilities
{
    public class AutoFacLoader
    {
        public static IContainer Container;
        public static void Initialize(HttpConfiguration config, Assembly assembly)
        {
            Initialize(config, RegisterServices(assembly, new ContainerBuilder()));
        }

        private static void Initialize(HttpConfiguration config, IContainer container)
        {
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static IContainer RegisterServices(Assembly assembly, ContainerBuilder builder)
        {
            builder.RegisterApiControllers(assembly);
            AutoFacRegistrar.RegisterTypes(ref builder);
            Container = builder.Build();
            return Container;
        }
    }
}
