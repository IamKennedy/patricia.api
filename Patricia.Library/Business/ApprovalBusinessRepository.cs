﻿using Patricia.Library.BusinessContracts;
using Patricia.Library.Common;
using Patricia.Library.CustomExceptions;
using Patricia.Library.DataContracts;
using Patricia.Library.DTO;
using Patricia.Library.Extensions;
using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;

using static Patricia.Library.Common.Enums;

namespace Patricia.Library.Business
{
    public class ApprovalBusinessRepository : IApprovalBusinessRepository
    {
        private readonly IApprovalDataRepository approvalDataRepository;
        private readonly IApprovalConfigDataRepository approvalConfigDataRepository;
        private readonly IIPDataRepository ipDataRepository;
        private readonly IUserBusinessRepository userBusinessRepository;
        private readonly IBinBusinessRepository binBusinessRepository;
        private readonly IBranchBusinessRepository branchBusinessRepository;
        private readonly ICardPRBusinessRepository cardPRBusinessRepository;
        private readonly IIPBusinessRepository ipBusinessRepository;
        private readonly IAuditTrailDataRepository auditDataRepository;
       


        public ApprovalBusinessRepository(
            IApprovalDataRepository approvalDataRepository,
            IApprovalConfigDataRepository approvalConfigDataRepository,
            IIPDataRepository ipDataRepository,
            IUserBusinessRepository userBusinessRepository,
            IBinBusinessRepository binBusinessRepository,
            IBranchBusinessRepository branchBusinessRepository,
            ICardPRBusinessRepository cardPRBusinessRepository,
            IIPBusinessRepository ipBusinessRepository,
            IAuditTrailDataRepository auditDataRepository

        )
        {
            this.approvalDataRepository = approvalDataRepository;
            this.approvalConfigDataRepository = approvalConfigDataRepository;
            this.ipDataRepository = ipDataRepository;
            this.userBusinessRepository = userBusinessRepository;
            this.binBusinessRepository = binBusinessRepository;
            this.branchBusinessRepository = branchBusinessRepository;
            this.cardPRBusinessRepository = cardPRBusinessRepository;
            this.ipBusinessRepository = ipBusinessRepository;
            this.auditDataRepository = auditDataRepository;
            
        }

        public List<ApprovalDTO> RetrieveAll()
        {
            var approvals = approvalDataRepository.RetrieveApprovals();
            return MapApprovals(approvals);
        }

        public List<ApprovalDTO> RetrieveFilteredApprovals(string fromDate, string toDate, string status, string approvalType)
        {
            var approvals = approvalDataRepository.RetrieveFilteredApprovals(fromDate, toDate, status, approvalType);
            return MapApprovals(approvals);
        }

        public ApprovalDTO RetrieveByUserName(string username)
        {
            ApprovalDTO approvalDTO;

            var approval = approvalDataRepository.RetrieveByUsername(username);

            if (approval != null)
            {
                approvalDTO = new ApprovalDTO()
                {
                    Id = approval.Id,                 
                    details = approval.Details,
                    oldDetails = approval.OldDetails,
                    requestedBy = approval.RequestedBy,
                    requestedOn = approval.RequestedOn.ToString(),
                    type = approval.Type,
                    approvedBy = approval.ApprovedBy,
                    approvedOn = approval.ApprovedOn.ToString(),
                };
                return approvalDTO;
            }
            return null;
        }

        public BaseDTO Update(ApprovalDTO approval)
        {
            if (approvalDataRepository.UpdateApproval(MapApprovalDTO(approval)))
            {
                if (approval.status == Enums.ApprovalStatus.APPROVED.ToString())
                {
                    var response = new BaseDTO();

                    response = UpdateObject(approval);

                    return response;
                }
                else
                {
                    return new BaseDTO
                    {
                        responseCode = "01",
                        responseMessage = "Could Not Update Approval Successfully"
                    };
                }
            }
            return null;
        }

        private BaseDTO UpdateObject(ApprovalDTO approval)
        {
            var _approval = approvalDataRepository.RetrieveById(approval.Id);

            var response = new BaseDTO();

            #region IP Approval Region
            if (approval.type.Equals(Enum.GetName(typeof(ApprovalType), ApprovalType.CreateIP)))
            {
                var obj = JsonConvert.DeserializeObject<IPDTO>(approval.details);
                var ip = new IP()
                {
                    Name = obj.name,
                    Description = obj.description,
                    IPAddress = obj.ipAddress
                };

                var isCreatIPApproved = ipDataRepository.Save(ip);

                if (isCreatIPApproved)
                {
                    var audits = new AuditTrail()
                    {
                        Type = "Create IP",
                        ApprovalDate = DateTime.Now,
                        ApprovedBy = approval.approvedBy,
                        RequestedBy = approval.requestedBy,
                        RequestedDate = approval.requestedOn.ToDate(),
                        Details = approval.details,
                        OldDetails = approval.oldDetails
                    };

                    auditDataRepository.Save(audits);

                    response.responseCode = "00";

                    response.responseMessage = $"IP Creation Request for {ip.IPAddress} Has Been Approved Successfully";
                    
                }
                else
                {
                    response.responseCode = "01";
                    response.responseMessage = $"IP Creation Approval Request for {ip.IPAddress} Failed";                  
                }
            }

            if (approval.type.Equals(Enum.GetName(typeof(ApprovalType), ApprovalType.UpdateIP)))
            {
                var obj = JsonConvert.DeserializeObject<IPDTO>(approval.details);
                var ip = new IP()
                {
                    Id = obj.id,
                    Name = obj.name,
                    Description = obj.description,
                    IPAddress = obj.ipAddress
                };

                var isUpdateIPApproved = ipDataRepository.Update(ip);

                if (isUpdateIPApproved)
                {
                    var audits = new AuditTrail()
                    {
                        Type = "Update IP",
                        ApprovalDate = DateTime.Now,
                        ApprovedBy = approval.approvedBy,
                        RequestedBy = approval.requestedBy,
                        RequestedDate = approval.requestedOn.ToDate(),
                        Details = approval.details,
                        OldDetails = approval.oldDetails
                    };

                    auditDataRepository.Save(audits);
                    response.responseCode = "00";
                    response.responseMessage = $"IP Update Request for {ip.IPAddress} Has Been Approved Successfully";
                   
                }
                else
                {
                    response.responseCode = "01";
                    response.responseMessage = $"IP Update Approval Request for {ip.IPAddress} Failed";
                    
                }
               
            
            }
            #endregion
            #region User Approval Region
            if (approval.type.Equals(Enum.GetName(typeof(ApprovalType), ApprovalType.CreateUser)))
            {
                var obj = JsonConvert.DeserializeObject<UsersDTO>(approval.details);
                obj.overrideApproval = true;

                var isUserApproved = userBusinessRepository.CreateUser(obj);

                if (isUserApproved.responseCode == "00")
                {
                    response.responseCode = "00";
                    response.responseMessage = $"User Creation Request for {obj.username} Has Been Approved Successfully";
                   
                }
                else
                {
                    var audits = new AuditTrail()
                    {
                        Type = "Create User",
                        ApprovalDate = DateTime.Now,
                        ApprovedBy = approval.approvedBy,
                        RequestedBy = approval.requestedBy,
                        RequestedDate = approval.requestedOn.ToDate(),
                        Details = approval.details,
                        OldDetails = approval.oldDetails
                    };

                    auditDataRepository.Save(audits);
                    response.responseCode = "01";
                    response.responseMessage = $"User Creation Approval Request for {obj.username} Failed";
                    
                }
            }

            if (approval.type.Equals(Enum.GetName(typeof(ApprovalType), ApprovalType.UpdateUser)))
            {
                var obj = JsonConvert.DeserializeObject<UsersDTO>(approval.details);

                var isUpdateUserApproved = userBusinessRepository.UpdateUser(obj);

                if (isUpdateUserApproved.responseCode == "00")
                {
                    var audits = new AuditTrail()
                    {
                        Type = "Update User",
                        ApprovalDate = DateTime.Now,
                        ApprovedBy = approval.approvedBy,
                        RequestedBy = approval.requestedBy,
                        RequestedDate = approval.requestedOn.ToDate(),
                        Details = approval.details,
                        OldDetails = approval.oldDetails
                    };

                    auditDataRepository.Save(audits);
                    response.responseCode = "00";
                    response.responseMessage = $"User Update Request for {obj.username} Has Been Approved Successfully";
                    
                }
                else
                {
                    response.responseCode = "01";
                    response.responseMessage = $"User Update Approval Request for {obj.username} Failed";
                    
                }


            }
            #endregion
            #region Bin Approval Region
            if (approval.type.Equals(Enum.GetName(typeof(ApprovalType), ApprovalType.CreateBin)))
            {
                var obj = JsonConvert.DeserializeObject<BinDTO>(approval.details);
                obj.overrideApproval = true;

                var isBinApproved = binBusinessRepository.AddBin(obj);

                if (isBinApproved.responseCode == "00")
                {
                    var audits = new AuditTrail()
                    {
                        Type = "Create Bin",
                        ApprovalDate = DateTime.Now,
                        ApprovedBy = approval.approvedBy,
                        RequestedBy = approval.requestedBy,
                        RequestedDate = approval.requestedOn.ToDate(),
                        Details = approval.details,
                        OldDetails = approval.oldDetails
                    };

                    auditDataRepository.Save(audits);
                    response.responseCode = "00";
                    response.responseMessage = $"Bin Creation Request for {obj.bin} Has Been Approved Successfully";
                    
                }
                else
                {
                    response.responseCode = "01";
                    response.responseMessage = $"Bin Creation Approval Request for {obj.bin} Failed";
                    
                }
            }

            if (approval.type.Equals(Enum.GetName(typeof(ApprovalType), ApprovalType.UpdateBin)))
            {
                var obj = JsonConvert.DeserializeObject<BinDTO>(approval.details);

                var isUpdateBinApproved = binBusinessRepository.UpdateBin(obj);

                if (isUpdateBinApproved.responseCode == "00")
                {
                    var audits = new AuditTrail()
                    {
                        Type = "Update Bin",
                        ApprovalDate = DateTime.Now,
                        ApprovedBy = approval.approvedBy,
                        RequestedBy = approval.requestedBy,
                        RequestedDate = approval.requestedOn.ToDate(),
                        Details = approval.details,
                        OldDetails = approval.oldDetails
                    };

                    auditDataRepository.Save(audits);
                    response.responseCode = "00";
                    response.responseMessage = $"Bin Update Request for {obj.bin} Has Been Approved Successfully";
                    
                }
                else
                {
                    response.responseCode = "01";
                    response.responseMessage = $"Bin Update Approval Request for {obj.bin} Failed";
                   
                }


            }
            #endregion
            #region Branch Approval Region
            if (approval.type.Equals(Enum.GetName(typeof(ApprovalType), ApprovalType.CreateBranch)))
            {
                var obj = JsonConvert.DeserializeObject<BranchDTO>(approval.details);
                obj.overrideApproval = true;

                var isBranchApproved = branchBusinessRepository.AddBranch(obj);

                if (isBranchApproved.responseCode == "00")
                {
                    var audits = new AuditTrail()
                    {
                        Type = "Create Branch",
                        ApprovalDate = DateTime.Now,
                        ApprovedBy = approval.approvedBy,
                        RequestedBy = approval.requestedBy,
                        RequestedDate = approval.requestedOn.ToDate(),
                        Details = approval.details,
                        OldDetails = approval.oldDetails
                    };

                    auditDataRepository.Save(audits);
                    response.responseCode = "00";
                    response.responseMessage = $"Branch Creation Request for {obj.name} Has Been Approved Successfully";
                    
                }
                else
                {
                    response.responseCode = "01";
                    response.responseMessage = $"Branch Creation Approval Request for {obj.name} Failed";
                    
                }
            }

            if (approval.type.Equals(Enum.GetName(typeof(ApprovalType), ApprovalType.UpdateBranch)))
            {
                var obj = JsonConvert.DeserializeObject<BranchDTO>(approval.details);
                obj.overrideApproval = true;

                var isUpdateBinApproved = branchBusinessRepository.UpdateBranch(obj);

                if (isUpdateBinApproved.responseCode == "00")
                {
                    var audits = new AuditTrail()
                    {
                        Type = "Update Branch",
                        ApprovalDate = DateTime.Now,
                        ApprovedBy = approval.approvedBy,
                        RequestedBy = approval.requestedBy,
                        RequestedDate = approval.requestedOn.ToDate(),
                        Details = approval.details,
                        OldDetails = approval.oldDetails
                    };

                    auditDataRepository.Save(audits);
                    response.responseCode = "00";
                    response.responseMessage = $"Branch Update Request for {obj.name} Has Been Approved Successfully";
                    
                }
                else
                {
                    response.responseCode = "01";
                    response.responseMessage = $"Branch Update Approval Request for {obj.name} Failed";
                    
                }


            }
            #endregion
            #region Card Production Region
            if (approval.type.Equals(Enum.GetName(typeof(ApprovalType), ApprovalType.ResetCardPrintStatus)))
            {
                var obj = JsonConvert.DeserializeObject<CardPRDTO>(approval.details);
                obj.overrideApproval = true;

                var isCardResetApproved = cardPRBusinessRepository.ResetCardRequest(obj);

                if (isCardResetApproved.responseCode == "00")
                {
                    response.responseCode = "00";
                    response.responseMessage = $"Card Request Reset for {obj.nameToPrint} Has Been Approved Successfully";
                    
                }
                else
                {
                    response.responseCode = "01";
                    response.responseMessage = $"Card Request Reset for {obj.nameToPrint} Failed";
                    
                }


            }
            #endregion

            AuditTrail audit = new AuditTrail()
            {
                Type = approval.type,
                Details = approval.details,
                RequestedBy = approval.requestedBy,
                RequestedDate = approval.requestedOn.ToDate(),
                ApprovedBy = approval.approvedBy,
                ApprovalDate = approval.approvedOn.ToDate()
            };

            auditDataRepository.Save(audit);
           
            return response;
        }

        public bool IsTypeConfiguredForApproval(string type)
        {
            var conf = approvalConfigDataRepository.RetrieveByType(type);
            if (conf == null)
            {
                throw new NotFoundException($"No approval configuration found for type {type}");
            }

            return conf.Approve.GetValueOrDefault(false);
        }

        private List<ApprovalDTO> MapApprovals(List<Approval> approvalList)
        {
            var approvalDto = new List<ApprovalDTO>();

            if (approvalList.Any())
            {
                foreach (var approval in approvalList)
                {
                    var _approvalDto = new ApprovalDTO()
                    {
                        Id = approval.Id,
                        type = approval.Type,
                        requestedBy = approval.RequestedBy,
                        status = approval.ApprovalStatus,
                        details = approval.Details,
                        oldDetails = approval.OldDetails,
                        approvedBy = approval.ApprovedBy,
                        requestedOn = approval.RequestedOn != null ? string.Format("{0:g}", approval.RequestedOn) : string.Empty,
                        approvedOn = approval.ApprovedOn != null ? string.Format("{0:g}", approval.ApprovedOn) : string.Empty
                    };
                    approvalDto.Add(_approvalDto);
                }
            }
            return approvalDto;
        }

        private Approval MapApprovalDTO(ApprovalDTO approval)
        {
            var _approval = new Approval()
            {
                Id = approval.Id,
                ApprovalStatus = approval.status,
                OldDetails = approval.oldDetails,
                Details = approval.details,
                RequestedBy = approval.requestedBy,
                RequestedOn = approval.requestedOn.ToDate(),
                ApprovedBy = approval.approvedBy,
                ApprovedOn = approval.approvedOn.ToDate(),
                Type = approval.type
            };
            return _approval;
        }

    }
}
