﻿
using System.Collections.Generic;
using System.Linq;
using System;
using System.Configuration;
using Patricia.Library.BusinessContracts;
using Patricia.Library.DataContracts;
using Patricia.Library.CommonContracts;
using Patricia.Library.DTO;
using Patricia.Library.Models;

namespace Patricia.Library.Business
{
    public class TransactionBusinessRepository : ITransactionBusinessRepository
    {
        private readonly ITransactionDataRepository transactionDataRespository;
        private readonly ICryptoLib cryptoLib;
        private readonly string eKey;

        public TransactionBusinessRepository(ITransactionDataRepository _transactionDataRepository, ICryptoLib cryptoLib)
        {
            transactionDataRespository = _transactionDataRepository;
            this.cryptoLib = cryptoLib;
            eKey = ConfigurationManager.AppSettings.Get("ekey");
        }

        public List<TransactionsDTO> RetrieveTransactions(string fromDate, string toDate)
        {
            var transactions = transactionDataRespository.RetrieveTransactions(fromDate, toDate);
            return MapTransactions(transactions);
        }
        
        public List<TransactionsDTO> RetrieveTransactionsByAccountNumber(string accountNumber)
        {
            var transactions = transactionDataRespository.RetrieveTransactionsByAccountNumber(accountNumber);
            return MapTransactions(transactions);
        }

        public List<TransactionsDTO> RetrieveTransactionsByCardNumber(string pan)
        {
            var transactions = transactionDataRespository.RetrieveTransactionsByCardNumber(cryptoLib.Encrypt(eKey, pan));
            return MapTransactions(transactions);
        }

        public TransactionsDTO RetrieveTransactionByReferenceID(string referenceID)
        {
            var transactionDto = new TransactionsDTO();
            var transaction = transactionDataRespository.RetrieveTransactionsByReferenceCode(referenceID);
            if (transaction != null)
            {
                transactionDto.amount = transaction.Amount;
                transactionDto.reference = transaction.Reference;
                transactionDto.serviceNarration = transaction.Narration;
                transactionDto.transactionDate = transaction.TransactionDate.ToString();
                transactionDto.transactionStatus = transaction.TransactionStatus;
            }
            return transactionDto;
        }

        public bool SaveTransaction(TransactionsDTO transaction)
        {
            var _transaction = new Transaction()
            {
                Narration = transaction.serviceNarration,
                Amount = transaction.amount,
                AccountNumber = transaction.accountNumber,
                TransactionDate = DateTime.Now,
                Reference = transaction.reference,
                TransactionStatus = transaction.transactionStatus,
                StatusCode = transaction.statusCode,
                Description = transaction.description
                
            };

            return transactionDataRespository.Save(_transaction);
        }

        private List<TransactionsDTO> MapTransactions(List<Transaction> transactions)
        {
            var transactionDto = new List<TransactionsDTO>();

            if (transactions.Any())
            {
                foreach (var transaction in transactions)
                {
                    var _transactionDto = new TransactionsDTO()
                    {
                        amount = transaction.Amount,
                        serviceNarration = transaction.Narration,
                        accountNumber = transaction.AccountNumber,
                        transactionDate = transaction.TransactionDate.ToString(),
                        reference = transaction.Reference,
                        transactionStatus = transaction.TransactionStatus,
                        description = transaction.Description,
                        statusCode = transaction.StatusCode
                };
                    transactionDto.Add(_transactionDto);
                }
            }
            return transactionDto;
        }

    }
}
