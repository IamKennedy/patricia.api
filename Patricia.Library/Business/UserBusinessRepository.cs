﻿using Newtonsoft.Json;
using Patricia.Library.BusinessContracts;
using Patricia.Library.Common;
using Patricia.Library.CustomExceptions;
using Patricia.Library.DataContracts;
using Patricia.Library.DTO;
using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static Patricia.Library.Common.Enums;

namespace Patricia.Library.Business
{
    public class UserBusinessRepository : IUserBusinessRepository
    {
        private readonly IUserDataRepository userDataRepository;
        private readonly IApprovalDataRepository approvalDataRepository;
        private readonly IApprovalConfigDataRepository approvalConfiDataRepository;
        private readonly IAuditTrailDataRepository auditTrailDataRepository;
        


        public UserBusinessRepository(IUserDataRepository userDataRepository, IApprovalDataRepository approvalDataRepository, IApprovalConfigDataRepository approvalConfiDataRepository, IAuditTrailDataRepository auditTrailDataRepository)
        {
            this.userDataRepository = userDataRepository;
            this.approvalDataRepository = approvalDataRepository;
            this.approvalConfiDataRepository = approvalConfiDataRepository;
            this.auditTrailDataRepository = auditTrailDataRepository;
        }

        public UserBusinessRepository()
        {

        }

        public UsersDTO ValidateUser(UsersDTO _user)
        {
            var user = userDataRepository.ValidateUser(_user.username, CryptoLib.SHA512(_user.password));

            if (user == null)
            {
                var userdto = new UsersDTO()
                {
                    responseCode = "01",
                    responseMessage = "Username or Password Seems to be Incorrect"
                };

                return userdto;
            }
            else
            {
                AuditTrail audit = new AuditTrail()
                {
                    Type = "User Login",
                    ClientIP = _user.clientIP,
                    RequestedBy = _user.operationUsername,
                    RequestedDate = DateTime.Now,
                    ApprovalDate = DateTime.Now                   
                };

                auditTrailDataRepository.Save(audit);

                return MapUser(user);
            }         
        }

        public UsersDTO ValidateUsername(UsersDTO _user)
        {
            var user = userDataRepository.ValidateUserWithoutPassword(_user.username);

            if (user == null)
            {
                var userdto = new UsersDTO()
                {
                    responseCode = "01",
                    responseMessage = "Username or Password Seems to be Incorrect"
                };

                return userdto;
            }
            else
            {
                AuditTrail audit = new AuditTrail()
                {
                    Type = "Password Reset",
                    ClientIP = _user.clientIP,
                    RequestedBy = _user.operationUsername,
                    RequestedDate = DateTime.Now,
                    ApprovalDate = DateTime.Now
                };

                auditTrailDataRepository.Save(audit);

                return MapUser(user);
            }
        }

        public UsersDTO RetrieveUserByUserName(UsersDTO _user)
        {
            var user = userDataRepository.RetriveUserByUsername(_user.username);
            if (user != null)
            {
                if (user.UserStatus == Enums.UserStatus.ACTIVE.ToString())
                {
                    return MapUser(user);
                }
                throw new NotFoundException("User account is not active. Kindly contact Patricia IT Support.");
            }
            throw new NotFoundException("User not found");
            
        }

        public UsersDTO RetrieveUserByID(UsersDTO _user)
        {
            var user = userDataRepository.RetriveUserByUserID(_user.id);
            return MapUser(user);
        }

        public List<UsersDTO> RetrieveUsers()
        {
            var users = userDataRepository.GetUsers();
            return MapUsers(users);
        }

        public BaseDTO ResetPassword (UsersDTO user)
        {
            BaseDTO response = new BaseDTO();

            var existingUser = userDataRepository.RetriveUserByUserID(user.id);

            UsersDTO _existingUser = new UsersDTO()
            {
                firstName = existingUser.OtherNames,
                lastName = existingUser.LastName,
                hashedPassword = null,
                email = existingUser.Email,
                firstTimeLogon = existingUser.FirstTimeLogon,
                role = existingUser.Role,
                username = existingUser.Username,
                userStatus = existingUser.UserStatus
            };

            if (!userDataRepository.Exists(user.username.ToLower()))
            {
                response.responseCode = "01";

                response.responseMessage = "The User to Update Does Not Exist";

                return response;
            }
            else
            {
                if (user.overrideApproval)
                {
                    var logForApproval = approvalConfiDataRepository.RetrieveByType("Reset Password").Approve;

                    if (logForApproval == true)
                    {
                        Approval approvalDTO = new Approval()
                        {
                            Type = Enum.GetName(typeof(ApprovalType), ApprovalType.ResetPassword),
                            RequestedBy = user.username,
                            RequestedOn = DateTime.Now,
                            Details = JsonConvert.SerializeObject(user),
                            OldDetails = JsonConvert.SerializeObject(_existingUser),
                            ApprovalStatus = Enums.ApprovalStatus.PENDING.ToString()
                        };

                        if (approvalDataRepository.Save(approvalDTO))
                        {
                            response.responseCode = "00";

                            response.responseMessage = "Update User Request Has Been Logged Successfully for Approval";

                            return response;
                        }
                        else
                        {
                            response.responseCode = "01";

                            response.responseMessage = "Operation Failed";

                            return response;
                        }
                    }
                }

                string password = CreateRandomPassword();

                User _user = new User()
                {
                    HashedPassword = CryptoLib.SHA512(password)
                };

                var isUserUpdated = userDataRepository.Update(_user);

                if (isUserUpdated)
                {
                    if (!user.overrideApproval)
                    {
                        AuditTrail audit = new AuditTrail()
                        {
                            Type = "Reset Password",
                            ClientIP = user.clientIP,
                            RequestedBy = user.operationUsername,
                            RequestedDate = DateTime.Now,
                            ApprovalDate = DateTime.Now,
                            ApprovedBy = user.approvedBy,
                            Details = JsonConvert.SerializeObject(_user),
                            OldDetails = JsonConvert.SerializeObject(existingUser)
                        };

                        auditTrailDataRepository.Save(audit);
                    }

                    response.responseCode = "00";
                    response.responseMessage = $"Password has been reset successfully, please check email for new password";
                    return response;
                }
                else
                {
                    response.responseCode = "01";
                    response.responseMessage = "Operation Failed";
                    return response;
                }
            }
        }

        public BaseDTO ForgotPassword(UsersDTO user)
        {
            BaseDTO response = new BaseDTO();

            User _user = userDataRepository.RetriveUserByUsername(user.username);

            if (_user != null)
            {
                if (_user.UserStatus == Enums.UserStatus.ACTIVE.ToString())
                {
                    string password = CreateRandomPassword();

                    _user.HashedPassword = CryptoLib.SHA512(password);

                    var isPasswordUpdated = userDataRepository.Update(_user);

                    if (isPasswordUpdated)
                    {
                        if (!user.overrideApproval)
                        {
                            var audit = new AuditTrail()
                            {
                                Type = "Reset Password",
                                ClientIP = user.clientIP,
                                RequestedBy = user.operationUsername,
                                RequestedDate = DateTime.Now,
                                ApprovalDate = DateTime.Now,
                                ApprovedBy = user.approvedBy,
                                Details = JsonConvert.SerializeObject(_user),
                                OldDetails = null
                            };

                            auditTrailDataRepository.Save(audit);

                            response.responseCode = "00";

                            UserEmail(user, password);

                            response.responseMessage = $"Password Has Been Reset Successfully, please check your registered Email Address";

                            return response;
                        }
                        else
                        {
                            response.responseCode = "01";

                            response.responseMessage = "Password Reset Failed";

                            return response;

                        }

                        throw new NotFoundException("User account is not active. Kindly contact Patricia IT Support.");
                    }
                    throw new NotFoundException("User not found");
                }
            }

            return null;
        }

        public BaseDTO CreateUser(UsersDTO user)
        {
            BaseDTO response = new BaseDTO();

            if (userDataRepository.Exists(user.username.ToLower()))
            {
                response.responseCode = "01";

                response.responseMessage = $"User : {(user.username)} Already Exists";

                return response;
            }
            else
            {
                if (user.overrideApproval)
                {
                    var logForApproval = approvalConfiDataRepository.RetrieveByType("Create User").Approve;

                    if (logForApproval == true)
                    {
                        Approval approvalDTO = new Approval()
                        {
                            Type = "Create User",
                            RequestedBy = user.operationUsername,
                            RequestedOn = DateTime.Now,
                            Details = JsonConvert.SerializeObject(user),
                            ApprovalStatus = Enums.ApprovalStatus.PENDING.ToString()
                        };

                        if (approvalDataRepository.Save(approvalDTO))
                        {
                            response.responseCode = "00";
                            response.responseMessage = "Add User Request Has Been Logged Successfully for Approval";
                            return response;
                        }
                        else
                        {
                            response.responseCode = "01";
                            response.responseMessage = "Operation Failed";
                            return response;
                        }
                    }
                }
                string password = CreateRandomPassword();
                string hashedPassword = CryptoLib.SHA512(password);
                User _user = new User()
                {
                    OtherNames = user.firstName,
                    LastName = user.lastName,
                    HashedPassword = hashedPassword,
                    Email = user.email,
                    FirstTimeLogon = true,
                    Role = user.role,
                    Username = user.username,
                    UserStatus = Enums.UserStatus.ACTIVE.ToString(),
                    Branch = user.branch,
                    CreatedOn = DateTime.Now,               
                };

                var isUserAdded = userDataRepository.Save(_user);

                UserEmail(user, password);

                if (isUserAdded)
                {
                    if (!user.overrideApproval)
                    {
                        var audit = new AuditTrail()
                        {
                            Type = "Create User",
                            ClientIP = user.clientIP,
                            RequestedBy = user.operationUsername,
                            RequestedDate = DateTime.Now,
                            ApprovalDate = DateTime.Now,
                            ApprovedBy = user.approvedBy,
                            Details = JsonConvert.SerializeObject(_user),
                            OldDetails = null
                        };

                        auditTrailDataRepository.Save(audit);
                    }
                    
                    response.responseCode = "00";
                    UserEmail(user, password);
                    response.responseMessage = $"User : {(user.username)} has been Added Successfully";
                    return response;
                }
                else
                {
                    response.responseCode = "01";
                    response.responseMessage = "Operation Failed";
                    return response;
                }
            }
        }

        private List<UsersDTO> MapUsers(List<User> users)
        {
            var userDto = new List<UsersDTO>();

            if (users.Any())
            {
                foreach (var user in users)
                {
                    var _userDto = new UsersDTO()
                    {
                        id = user.Id,
                        email = user.Email,
                        firstName = user.OtherNames,
                        firstTimeLogon = user.FirstTimeLogon,
                        lastName = user.LastName,
                        role = user.Role,
                        username = user.Username,
                        userStatus = user.UserStatus,
                        branch = user.Branch
                        
                    };
                    userDto.Add(_userDto);
                }
            }
            return userDto;
        }

        public BaseDTO UpdateUser(UsersDTO userDto)
        {

            var existingUser = userDataRepository.RetriveUserByUserID(userDto.id);

            UsersDTO _existingUser = new UsersDTO()
            {
                firstName = existingUser.OtherNames,
                lastName = existingUser.LastName,
                hashedPassword = null,
                email = existingUser.Email,
                firstTimeLogon = existingUser.FirstTimeLogon,
                role = existingUser.Role,
                username = existingUser.Username,
                userStatus = existingUser.UserStatus
            };

            BaseDTO response = new BaseDTO();

           
            
            if (userDto.overrideApproval)
            {
                var logForApproval = approvalConfiDataRepository.RetrieveByType("Update User").Approve;

                if (logForApproval == true)
                {
                    Approval approvalDTO = new Approval()
                    {
                        Type = Enum.GetName(typeof(ApprovalType), ApprovalType.UpdateUser),
                        RequestedBy = userDto.username,
                        RequestedOn = DateTime.Now,
                        Details = JsonConvert.SerializeObject(userDto),
                        OldDetails = JsonConvert.SerializeObject(_existingUser),
                        ApprovalStatus = Enums.ApprovalStatus.PENDING.ToString()
                    };

                    if (approvalDataRepository.Save(approvalDTO))
                    {
                        response.responseCode = "00";

                        response.responseMessage = "Update User Request Has Been Logged Successfully for Approval";

                        return response;
                    }
                    else
                    {
                        response.responseCode = "01";

                        response.responseMessage = "Operation Failed";

                        return response;
                    }
                }
            }

            User _user = new User()
            {
                Id = userDto.id,
                OtherNames = userDto.firstName,
                LastName = userDto.lastName,
                Email = userDto.email,
                FirstTimeLogon = true,
                Role = userDto.role,
                Username = userDto.username,
                UserStatus = userDto.userStatus,
                Branch = userDto.branch
            };

            var isUserUpdated = userDataRepository.Update(_user);

            if (isUserUpdated)
            {
                    
                AuditTrail audit = new AuditTrail()
                {
                    Type = "Update User",
                    ClientIP = userDto.clientIP,
                    RequestedBy = userDto.operationUsername,
                    RequestedDate = DateTime.Now,
                    ApprovalDate = DateTime.Now,
                    ApprovedBy = userDto.approvedBy,
                    Details = JsonConvert.SerializeObject(_user),
                    OldDetails = JsonConvert.SerializeObject(existingUser)
                };

                auditTrailDataRepository.Save(audit);             
                response.responseCode = "00";
                response.responseMessage = $"User has been Updated Successfully";
                return response;
            }
            else
            {
                response.responseCode = "01";
                response.responseMessage = "Operation Failed";
                return response;
            }
                     
        }

        public void ThrowIfUserExists(UsersDTO userName)
        {
            if (userDataRepository.Exists(userName.username))
            {
                throw new InvalidException($"User with username: {userName.username} exists.");
            }
        }

        private void UserEmail(UsersDTO user, string password)
        {
            string fromMailAddress = ConfigurationManager.AppSettings["FromMailAddress"];
            string smtpPassword = ConfigurationManager.AppSettings["smtppassword"];
            string notifyEmailPath = System.Web.Hosting.HostingEnvironment.MapPath(@"/App_Data/Newuser-notify-email.txt");
            string smtpHost = ConfigurationManager.AppSettings["smtpHost"];
            string smtpPort = ConfigurationManager.AppSettings["SmtpPort"];
            string url = ConfigurationManager.AppSettings["WebsiteUrl"];
            string htmlstring = System.IO.File.ReadAllText(notifyEmailPath);
            htmlstring = htmlstring.Replace("First_Name", user.firstName);
            htmlstring = htmlstring.Replace("user_name", user.username);
            htmlstring = htmlstring.Replace("pass_word", password);
            htmlstring = htmlstring.Replace("url_link", url);


            SmtpClient client = new SmtpClient(smtpHost, Convert.ToInt32(smtpPort));
            client.EnableSsl = true;
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(fromMailAddress, smtpPassword);
            MailMessage mailMessage = new MailMessage();
            mailMessage.To.Add(user.email);
            mailMessage.From = new MailAddress(fromMailAddress);
            mailMessage.Subject = "PATRICIA NOTIFICATIONS";
            mailMessage.Body = htmlstring;
            mailMessage.IsBodyHtml = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
            client.Send(mailMessage);
        }

        private static string CreateRandomPassword(int length = 15)
        {
            // Create a string of characters, numbers, special characters that allowed in the password  
            string validChars = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*?_-";
            Random random = new Random();

            // Select one random character at a time from the string  
            // and create an array of chars  
            char[] chars = new char[length];
            for (int i = 0; i < length; i++)
            {
                chars[i] = validChars[random.Next(0, validChars.Length)];
            }
            return new string(chars);
        }

        public BaseDTO ChangePassword(PasswordDTO passwordDTO)
        {
            BaseDTO response = new BaseDTO();
                    
            var validatedUser = userDataRepository.ValidateUser(passwordDTO.username, CryptoLib.SHA512(passwordDTO.oldpassword));

            if (validatedUser == null)
            {
                response.responseCode = "01";
                response.responseMessage = "Incorrect Password";
                return response;
            }
            else
            {
                validatedUser.HashedPassword = CryptoLib.SHA512(passwordDTO.newPassword);

                var isUserUpdated = userDataRepository.Update(validatedUser);

                if (isUserUpdated)
                {
                    response.responseCode = "00";
                    response.responseMessage = "Password Changed Successfully";
                    return response;
                }
            }
            return null;
        }

        private UsersDTO MapUser(User user)
        {
            var user_ = new UsersDTO();
                  
            if (user != null)
            {
                var _user = new UsersDTO()
                {
                    id = user.Id,
                    email = user.Email,
                    firstName = user.OtherNames,
                    firstTimeLogon = user.FirstTimeLogon,
                    lastName = user.LastName,
                    role = user.Role,
                    username = user.Username,
                    userStatus = user.UserStatus,
                    branch = user.Branch
                };

                user_  =  _user;      
            }
            return user_;
        }

    }
}
