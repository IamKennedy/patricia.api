﻿using Newtonsoft.Json;
using Patricia.Library.BusinessContracts;
using Patricia.Library.Common;
using Patricia.Library.DataContracts;
using Patricia.Library.DTO;
using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using static Patricia.Library.Common.Enums;

namespace Patricia.Library.Business
{
    public class IPBusinessRepository : IIPBusinessRepository
    {
        private readonly IIPDataRepository ipDataRepository;
        private readonly IApprovalDataRepository approvalDataRepository;
        private readonly IApprovalConfigDataRepository approvalConfiDataRepository;
        private readonly IAuditTrailDataRepository auditTrailDataRepository;

        public IPBusinessRepository(
           IIPDataRepository ipDataRepository,
           IApprovalDataRepository approvalDataRepository,
           IApprovalConfigDataRepository approvalConfiDataRepository,
           IAuditTrailDataRepository auditTrailDataRepository

           )
        {
            this.approvalConfiDataRepository = approvalConfiDataRepository;
            this.approvalDataRepository = approvalDataRepository;
            this.ipDataRepository = ipDataRepository;
            this.auditTrailDataRepository = auditTrailDataRepository;
        }

        public List<IPDTO> RetrieveIPs()
        {
            var ips = ipDataRepository.RetrieveIPS();

            return (from i in ips
                    select new IPDTO()
                    {
                        id = i.Id,
                        ipAddress = i.IPAddress,
                        name = i.Name,
                        description = i.Description,
                        ipStatus = i.IPStatus.ToString()
                    }).ToList();
        }

        public BaseDTO AddIP(IPDTO ip)
        {
            BaseDTO response = new BaseDTO();

            if (DoesIPExist(ip.ipAddress))
            {
                response.responseCode = "01";

                response.responseMessage = $"IP : {(ip.ipAddress)} Already Exists";

                return response;
            }
            else
            {
                if (ip.overrideApproval)
                {
                    var logForApproval = approvalConfiDataRepository.RetrieveByType("Create IP").Approve;

                    if (logForApproval == true)
                    {
                        Approval approvalDTO = new Approval()
                        {
                            Type = Enum.GetName(typeof(ApprovalType), ApprovalType.CreateIP),
                            RequestedBy = ip.operatingUser,
                            RequestedOn = DateTime.Now,
                            Details = JsonConvert.SerializeObject(ip),
                            ApprovalStatus = Enums.ApprovalStatus.PENDING.ToString()
                        };

                        if (approvalDataRepository.Save(approvalDTO))
                        {
                            response.responseCode = "00";
                            response.responseMessage = "Add IP Request Has Been Logged Successfully for Approval";
                            return response;
                        }
                        else
                        {
                            response.responseCode = "01";
                            response.responseMessage = "Operation Failed";
                            return response;
                        }
                    }
                }

                IP _ip = new IP()
                {
                    Name = ip.name,
                    IPAddress = ip.ipAddress,
                    Description = ip.description,
                    IPStatus = true        
                };

                var isIPAdded = ipDataRepository.Save(_ip);

                if (isIPAdded)
                {
                    var audit = new AuditTrail()
                    {
                        Type = "Create IP",
                        RequestedBy = ip.operatingUser,
                        RequestedDate = DateTime.Now,
                        ApprovalDate = DateTime.Now,
                        ApprovedBy = ip.approvedBy,
                        Details = JsonConvert.SerializeObject(_ip),
                        OldDetails = null
                    };

                    auditTrailDataRepository.Save(audit);

                    response.responseCode = "00";
                    response.responseMessage = $"IP : {(ip.ipAddress)} has been Added Successfully";
                    return response;
                }
                else
                {
                    response.responseCode = "01";
                    response.responseMessage = "Operation Failed";
                    return response;
                }
            }
           
        }

        public BaseDTO UpdateIP(IPDTO ip)
        {

            BaseDTO response = new BaseDTO();

            var existingIP = ipDataRepository.RetrieveIPByID(ip.id);

            IPDTO _existingIP = new IPDTO()
            {
                id = existingIP.Id,
                ipAddress = existingIP.IPAddress,
                name = existingIP.Name,
                description = existingIP.Description,
                ipStatus = existingIP.IPStatus.ToString()

            };

            if (!ipDataRepository.Exists(existingIP.IPAddress))
            {
                response.responseCode = "01";

                response.responseMessage = "The IP to Update Does Not Exist";

                return response;
            }
            else
            {
                if (ip.overrideApproval)
                {
                    var logForApproval = approvalConfiDataRepository.RetrieveByType("Update IP").Approve;

                    if (logForApproval == true)
                    {
                        Approval approvalDTO = new Approval()
                        {
                            Type = Enum.GetName(typeof(ApprovalType), ApprovalType.UpdateIP),
                            RequestedBy = ip.operatingUser,
                            RequestedOn = DateTime.Now,
                            Details = JsonConvert.SerializeObject(ip),
                            OldDetails = JsonConvert.SerializeObject(_existingIP),
                            ApprovalStatus = Enums.ApprovalStatus.PENDING.ToString()
                        };

                        if (approvalDataRepository.Save(approvalDTO))
                        {
                            response.responseCode = "00";

                            response.responseMessage = "Update IP Request Has Been Logged Successfully for Approval";

                            return response;
                        }
                        else
                        {
                            response.responseCode = "01";

                            response.responseMessage = "Operation Failed";
                        }
                    }
                }

                IP _ip = new IP()
                {
                    Id = ip.id,
                    Name = ip.name,
                    IPAddress = ip.ipAddress,
                    Description = ip.description,
                    IPStatus = Boolean.Parse(ip.ipStatus)
                };

                var isIPAdded = ipDataRepository.Update(_ip);

                if (isIPAdded)
                {
                    var audit = new AuditTrail()
                    {
                        Type = "Update IP",
                        RequestedBy = ip.operatingUser,
                        RequestedDate = DateTime.Now,
                        ApprovalDate = DateTime.Now,
                        ApprovedBy = ip.approvedBy,
                        Details = JsonConvert.SerializeObject(_ip),
                        OldDetails = JsonConvert.SerializeObject(_existingIP)
                    };

                    auditTrailDataRepository.Save(audit);

                    response.responseCode = "00";
                    response.responseMessage = $"IP : {(ip.ipAddress)} has been Updated Successfully";
                    return response;
                }
                else
                {
                    response.responseCode = "01";
                    response.responseMessage = "Operation Failed";
                    return response;
                }
            }
            
        }

        public bool DoesIPExist(string ip)
        {
            if (ipDataRepository.Exists(ip))
            {
                return true;
            }
            return false;
        }
    }
}
