﻿
using System.Configuration;
using Patricia.Library.BusinessContracts;
using Patricia.Library.DataContracts;
using Patricia.Library.CommonContracts;
using Patricia.Library.DTO;
using Patricia.Library.Models;
using System.Globalization;
using System;
using Newtonsoft.Json;
using Patricia.Library.Common;

namespace Patricia.Library.Business
{
    public class CardBusinessRepository : ICardBusinessRepository
    {
        private readonly ICardDataRepository cardDataRepository;
        private readonly IAuditTrailDataRepository auditTrailDataRepository;
        private readonly IApprovalDataRepository approvalDataRepository;
        
        private readonly ICryptoLib crypto;
        private readonly string eKey;
        public CardBusinessRepository(
            ICardDataRepository _cardDataRepository, 
            ICryptoLib _crypto, 
            IAuditTrailDataRepository _auditTrailDataRepository,
            IApprovalDataRepository _approvalDataRepository
            )
        {
            cardDataRepository = _cardDataRepository;
            auditTrailDataRepository = _auditTrailDataRepository;
            approvalDataRepository = _approvalDataRepository;
            crypto = _crypto;
            eKey = ConfigurationManager.AppSettings["ekey"];
        }

        public CardDTO GetCardInfo(PanDTO cardPan)
        {
            var cardDto = new CardDTO();
            var cardInfo = cardDataRepository.GetCardByAccountNumber(cardPan.accountNumber);
            if (cardInfo != null)
            {
                cardDto.Id = cardInfo.Id;
                cardDto.accountName = cardInfo.AccountName;
                cardDto.accountNumber = cardInfo.AccountNumber;
                cardDto.cardExpiryDate = cardInfo.CardExpiryDate;
                cardDto.cardStatus = cardInfo.CardStatus.ToString();
                cardDto.cardNumber = crypto.Decrypt(eKey, cardInfo.CardNumber);
            }
            return cardDto;
        }

        public BaseDTO BlockCard(PanDTO card)
        {
            string encryptedCard = crypto.Encrypt(eKey, card.cardNumber);

            BaseDTO response = new BaseDTO();

            var carddetails = cardDataRepository.GetCardByPan(encryptedCard);

            if (carddetails != null)
            {
                if (!card.isBlockCardConfiguredForApproval)
                {
                    var isCardBlocked = cardDataRepository.Block(carddetails.CardNumber);

                    if (isCardBlocked)
                    {
                        AuditTrail auditTrail = new AuditTrail()
                        {
                            Type = "Block Card",
                            RequestedBy = card.requestedBy,
                            RequestedDate = DateTime.Now,
                            ApprovalDate = DateTime.Now,
                            ApprovedBy = card.approvedBy,
                            Details = JsonConvert.SerializeObject(carddetails),
                            OldDetails = null
                        };

                        auditTrailDataRepository.Save(auditTrail);

                        response.responseCode = "00";
                        response.responseMessage = "Card is Blocked Successfully";

                        return response;
                    }
                    else
                    {
                        response.responseCode = "01";
                        response.responseMessage = "Card Could Not Be Blocked";

                        return response;
                    }
                }
                else
                {
                    Approval approvalDTO = new Approval()
                    {
                        Type = "Block Card",
                        RequestedBy = card.requestedBy,
                        RequestedOn = DateTime.Now,
                        Details = JsonConvert.SerializeObject(carddetails),
                        ApprovalStatus = Enums.ApprovalStatus.PENDING.ToString()
                    };

                    if (approvalDataRepository.Save(approvalDTO))
                    {
                        response.responseCode = "00";
                        response.responseMessage = "Block Card Request Has Been Logged Successfully for Approval";
                        return response;
                    }
                    else
                    {
                        response.responseCode = "01";
                        response.responseMessage = "Operation Failed";
                        return response;
                    }
                }
            }
            else
            {
                response.responseCode = "01";
                response.responseMessage = "Operation Failed, could not retrieved card details. Ensure Card number inserted is correct";
                return response;
            }
            
        }

        public BaseDTO UnBlockCard(PanDTO card)
        {
            string encryptedCard = crypto.Encrypt(eKey, card.cardNumber);

            BaseDTO response = new BaseDTO();

            var carddetails = cardDataRepository.GetCardByPan(encryptedCard);

            if (carddetails != null)
            {
                if (!card.isUnBlockCardConfiguredForApproval)
                {
                    var isCardBlocked = cardDataRepository.Unblock(carddetails.CardNumber);

                    if (isCardBlocked)
                    {
                        AuditTrail auditTrail = new AuditTrail()
                        {
                            Type = "UnBlock Card",
                            RequestedBy = card.requestedBy,
                            RequestedDate = DateTime.Now,
                            ApprovalDate = DateTime.Now,
                            ApprovedBy = card.approvedBy,
                            Details = JsonConvert.SerializeObject(carddetails),
                            OldDetails = null
                        };

                        auditTrailDataRepository.Save(auditTrail);

                        response.responseCode = "00";
                        response.responseMessage = "Card is UnBlocked Successfully";

                        return response;
                    }
                    else
                    {
                        response.responseCode = "01";
                        response.responseMessage = "Card Could Not Be UnBlocked";

                        return response;
                    }
                }
                else
                {
                    Approval approvalDTO = new Approval()
                    {
                        Type = "UnBlock Card",
                        RequestedBy = card.requestedBy,
                        RequestedOn = DateTime.Now,
                        Details = JsonConvert.SerializeObject(carddetails),
                        ApprovalStatus = Enums.ApprovalStatus.PENDING.ToString()
                    };

                    if (approvalDataRepository.Save(approvalDTO))
                    {
                        response.responseCode = "00";
                        response.responseMessage = "UnBlock Card Request Has Been Logged Successfully for Approval";
                        return response;
                    }
                    else
                    {
                        response.responseCode = "01";
                        response.responseMessage = "Operation Failed";
                        return response;
                    }
                }
            }
            else
            {
                response.responseCode = "01";
                response.responseMessage = "Operation Failed, could not retrieved card details. Ensure Card number inserted is correct";
                return response;
            }

        }

        public BaseDTO LinkCard(CardDTO card)
        {
            string encryptedCard = crypto.Encrypt(eKey, card.cardNumber);

            BaseDTO response = new BaseDTO();

            var carddetails = cardDataRepository.GetCardByPan(encryptedCard);

            var isThisAccountActive = cardDataRepository.GetActiveCardByAccountNumber(card.accountNumber);

            if (isThisAccountActive != null)
            {
                response.responseCode = "01";
                response.responseMessage = "Account Holder Already Has an Existing Active Card, Please Block/Hotlist before issuing a new Card.";
                return response;
            }

            if (carddetails == null)
            {
                if (!card.isLinkCardConfiguredForApproval)
                {
                    card.cardNumber = encryptedCard;

                    var isCardLinked = cardDataRepository.Save(MapCard(card));

                    if (isCardLinked)
                    {
                        AuditTrail auditTrail = new AuditTrail()
                        {
                            Type = "Link Card",
                            RequestedBy = card.requestedBy,
                            RequestedDate = DateTime.Now,
                            ApprovalDate = DateTime.Now,
                            ApprovedBy = card.approvedBy,
                            Details = JsonConvert.SerializeObject(MapCard(card)),
                            OldDetails = null
                        };

                        auditTrailDataRepository.Save(auditTrail);

                        response.responseCode = "00";
                        response.responseMessage = "Card is Linked Successfully";

                        return response;
                    }
                    else
                    {
                        response.responseCode = "01";
                        response.responseMessage = "Card Could Not Be Linked";

                        return response;
                    }
                }
                else
                {
                    Approval approvalDTO = new Approval()
                    {
                        Type = "Link Card",
                        RequestedBy = card.requestedBy,
                        RequestedOn = DateTime.Now,
                        Details = JsonConvert.SerializeObject(carddetails),
                        ApprovalStatus = Enums.ApprovalStatus.PENDING.ToString()
                    };

                    if (approvalDataRepository.Save(approvalDTO))
                    {
                        response.responseCode = "00";
                        response.responseMessage = "Link Card Request Has Been Logged Successfully for Approval";
                        return response;
                    }
                    else
                    {
                        response.responseCode = "01";
                        response.responseMessage = "Operation Failed";
                        return response;
                    }
                }
            }
            else
            {
                response.responseCode = "01";
                response.responseMessage = "Card Is Already Linked. Please use another Card.";
                return response;
            }
        }

        private Card MapCard(CardDTO card)
        {
            var _card = new Card()
            {
                AccountName = card.accountName,
                CardNumber = card.cardNumber,
                AccountNumber = card.accountNumber,
                CardExpiryDate = card.cardExpiryDate,
                CardStatus = "ACTIVE",
                Partner = "PATRICIA",
                DateLinked = DateTime.Now,
            };
            return _card;
        }
      
    }
}
