﻿using Newtonsoft.Json;
using Patricia.Library.BusinessContracts;
using Patricia.Library.Common;
using Patricia.Library.DataContracts;
using Patricia.Library.DTO;
using Patricia.Library.Extensions;
using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Patricia.Library.Common.Enums;

namespace Patricia.Library.Business
{
    public class CardPRBusinessRepository : ICardPRBusinessRepository
    {
        private readonly ICardPRDataRepository cardRequestDataRepository;
        private readonly IApprovalDataRepository approvalDataRepository;
        private readonly IApprovalConfigDataRepository approvalConfiDataRepository;
        private readonly IUserDataRepository userDataRepository;

        public CardPRBusinessRepository(
            ICardPRDataRepository cardRequestDataRepository,
            IApprovalDataRepository approvalDataRepository,
            IApprovalConfigDataRepository approvalConfiDataRepository,
            IUserDataRepository userDataRepository)
        {
            this.cardRequestDataRepository = cardRequestDataRepository;
            this.approvalConfiDataRepository = approvalConfiDataRepository;
            this.approvalDataRepository = approvalDataRepository;
            this.userDataRepository = userDataRepository;
        }
        public List<CardPRDTO> RetrieveAll()
        {           
            var printRequests = cardRequestDataRepository.RetrievePrintRequests();
            return MapPrintRequests(printRequests);          
        }

        public List<CardPRDTO> RetrieveFilteredPrintRequest(string fromDate, string toDate, string printStatus, string branchName)
        {
            var printRequests = cardRequestDataRepository.RetrieveFilteredRequests(fromDate, toDate, printStatus, branchName);
            return MapPrintRequests(printRequests);
        }

        public BaseDTO UpdateCardRequest(CardPRDTO printRequest, string username)
        {
            var user = userDataRepository.RetriveUserByUsername(username);

            var oldPrintRequest = cardRequestDataRepository.GetPrintRequestbyId(printRequest.id);

            CardPrintRequest _oldPrintRequest = new CardPrintRequest()
            {
                Id = printRequest.id,
                AccountNumber = oldPrintRequest.AccountNumber,
                isMobile = oldPrintRequest.isMobile,
                NameToPrint = oldPrintRequest.NameToPrint,
                Status = oldPrintRequest.Status,
                BranchName = user != null ? user.Branch : null,
                DateRequested = oldPrintRequest.DateRequested,
                DateApproved = oldPrintRequest.DateApproved
            };

            CardPrintRequest _printRequest = new CardPrintRequest()
            {
                Id = printRequest.id,
                AccountNumber = printRequest.accountNumber,
                isMobile = printRequest.isMobile,
                NameToPrint = printRequest.nameToPrint,
                Status = PrintRequestStatus.PENDING.ToString(),
                BranchName = user != null ? user.Branch : null,
                DateRequested = printRequest.requestedDate.ToDate(),
                DateApproved = printRequest.requestedDate.ToDate()
            };

            BaseDTO cardRequestDTO = new BaseDTO();

            var isCardRequestUpdateSuccessful = cardRequestDataRepository.UpdatePrintRequest(_printRequest, _oldPrintRequest);

            if (isCardRequestUpdateSuccessful)
            {
                cardRequestDTO.responseCode = "00";
                cardRequestDTO.responseMessage = "Card Request Status Updated Successfully";
            }
            else
            {
                cardRequestDTO.responseCode = "01";
                cardRequestDTO.responseMessage = "Card Request Status Update Failed";
            }
            return cardRequestDTO;
        }

        public BaseDTO ResetCardRequest(CardPRDTO printRequest)
        {
            var user = userDataRepository.RetriveUserByUsername(printRequest.requestedBy);

            var oldPrintRequest = cardRequestDataRepository.GetPrintRequestbyId(printRequest.id);

            CardPrintRequest _oldPrintRequest = new CardPrintRequest()
            {
                Id = printRequest.id,
                AccountNumber = oldPrintRequest.AccountNumber,
                isMobile = oldPrintRequest.isMobile,
                NameToPrint = oldPrintRequest.NameToPrint,
                Status = oldPrintRequest.Status,
                BranchName = user != null ? user.Branch : null,
                DateRequested = oldPrintRequest.DateRequested,
                DateApproved = oldPrintRequest.DateApproved
            };

            CardPrintRequest _printRequest = new CardPrintRequest()
            {
                Id = printRequest.id,
                AccountNumber = printRequest.accountNumber,
                isMobile = printRequest.isMobile,
                NameToPrint = printRequest.nameToPrint,
                Status = PrintRequestStatus.PENDING.ToString(),
                BranchName = user != null ? user.Branch : null,
                DateRequested = printRequest.requestedDate.ToDate(),
                DateApproved = printRequest.requestedDate.ToDate()
            };

            if (_printRequest.isMobile == true)
            {
                printRequest.overrideApproval = true;
                _printRequest.RequestBy = $"MOBILE : { _printRequest.AccountNumber}";
                _printRequest.ApprovedBy = _printRequest.ApprovedBy;

            }

            BaseDTO response = new BaseDTO();

            if (!printRequest.overrideApproval)
            {
                bool? logForApproval = approvalConfiDataRepository.RetrieveByType(Enum.GetName(typeof(ApprovalType), ApprovalType.InsertedPrintRecords)).Approve;

                if (logForApproval == true)
                {
                    Approval approvalDTO = new Approval()
                    {
                        Type = Enum.GetName(typeof(ApprovalType), ApprovalType.ResetCardPrintStatus),
                        RequestedBy = printRequest.requestedBy,
                        RequestedOn = DateTime.Now,
                        Details = JsonConvert.SerializeObject(_printRequest),
                        ApprovalStatus = ApprovalStatus.PENDING.ToString(),
                    };

                    if (approvalDataRepository.Save(approvalDTO))
                    {
                        response.responseCode = "00";
                        response.responseMessage = "Card Reset Request Has Been Logged Successfully for Approval";
                        return response;
                    }
                    else
                    {
                        response.responseCode = "01";
                        response.responseMessage = "Operation Failed";
                        return response;
                    }
                }
            }


            var isRequestLogged = cardRequestDataRepository.UpdatePrintRequest(_printRequest,oldPrintRequest);

            if (isRequestLogged)
            {
                response.responseCode = "00";
                response.responseMessage = "Print Request Has Been Reset Successfully";
                return response;

            }
            else
            {
                response.responseCode = "01";
                response.responseMessage = "Operation Failed";
                return response;
            }

            
        }

        public BaseDTO LogCardRequest(CardPRDTO printRequest)
        {
            
            BaseDTO response = new BaseDTO();

            CardPrintRequest _printRequest = new CardPrintRequest()
            {
                AccountNumber = printRequest.accountNumber,
                isMobile = printRequest.isMobile,
                NameToPrint = printRequest.nameToPrint,
                Status = PrintRequestStatus.PENDING.ToString(),
                BranchName = null,
                DateRequested = printRequest.requestedDate.ToDate(),
                DateApproved = printRequest.requestedDate.ToDate()
            };

            if (_printRequest.isMobile == true)
            {
                printRequest.overrideApproval = true;
                _printRequest.RequestBy = $"MOBILE : { _printRequest.AccountNumber}";
                _printRequest.ApprovedBy = "MOBILE";
            }
            else
            {
                printRequest.overrideApproval = true;
                _printRequest.RequestBy = $"WEB : { _printRequest.AccountNumber}";
                _printRequest.ApprovedBy = "WEB";
            }

            if (!printRequest.overrideApproval)
            {
                bool? logForApproval = approvalConfiDataRepository.RetrieveByType(Enum.GetName(typeof(ApprovalType), ApprovalType.InsertedPrintRecords)).Approve;

                if (logForApproval == true)
                {
                    Approval approvalDTO = new Approval()
                    {
                        Type = Enum.GetName(typeof(ApprovalType), ApprovalType.InsertedPrintRecords),
                        RequestedBy = $"WEB : { _printRequest.AccountNumber}",
                        RequestedOn = DateTime.Now,
                        Details = JsonConvert.SerializeObject(_printRequest),
                        ApprovalStatus = ApprovalStatus.PENDING.ToString(),
                    };

                    if (approvalDataRepository.Save(approvalDTO))
                    {
                        response.responseCode = "00";
                        response.responseMessage = "Card Request Has Been Logged Successfully for Approval";
                        return response;
                    }
                    else
                    {
                        response.responseCode = "01";
                        response.responseMessage = "Operation Failed";
                        return response;
                    }
                }
            }
           
            
            var isRequestLogged = cardRequestDataRepository.Save(_printRequest);

            if (isRequestLogged)
            {
                response.responseCode = "00";
                response.responseMessage = "Print Request Has Been Logged Successfully";
                return response;

            }
            else
            {
                response.responseCode = "01";
                response.responseMessage = "Operation Failed";
                return response;
            }

        }

        public CardPRDTO CheckCardRequestStatus(long printrequestId)
        { 
            var printRequest = cardRequestDataRepository.GetPrintRequestbyId(printrequestId);

            if (printRequest == null)
            {
                CardPRDTO _printRequest = new CardPRDTO()
                {
                    responseMessage = $"Print Request Id : {printrequestId} does not exist",
                    responseCode = "01"
                };
               
            }
            CardPRDTO printRequestDto = new CardPRDTO()
            {
                accountNumber = printRequest.AccountNumber,
                nameToPrint = printRequest.NameToPrint,
                printStatus = printRequest.Status.ToString(),
                printedOn = printRequest.DatePrinted.ToString(),
                id = printRequest.Id,
                isMobile = printRequest.isMobile,
                requestedDate = printRequest.DateRequested.ToString(),
                responseMessage = $"Print Request Retrieved Successfully",
                responseCode = "00"
            };

            return printRequestDto;


        }

        private List<CardPRDTO> MapPrintRequests(List<CardPrintRequest> printRequests)
        {
            var printRequestDto = new List<CardPRDTO>();

            if (printRequestDto.Any())
            {
                foreach (var printRequest in printRequests)
                {
                    var _printRequestDto = new CardPRDTO()
                    {
                        id = printRequest.Id,
                        printStatus = printRequest.Status,
                        approvalDate = printRequest.DateApproved.ToString(),
                        requestedBy = printRequest.RequestBy,
                        approvedBy = printRequest.ApprovedBy,
                        requestedDate = printRequest.DateRequested.ToString(),
                        nameToPrint = printRequest.NameToPrint,
                        isMobile = printRequest.isMobile,
                        printedOn = printRequest.DatePrinted.ToString(),
                    };
                    printRequestDto.Add(_printRequestDto);
                }
            }
            return printRequestDto;
        }

            
    }
}
