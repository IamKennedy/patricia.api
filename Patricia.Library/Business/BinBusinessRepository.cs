﻿using Newtonsoft.Json;
using Patricia.Library.BusinessContracts;
using Patricia.Library.Common;
using Patricia.Library.DataContracts;
using Patricia.Library.DTO;
using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using static Patricia.Library.Common.Enums;

namespace Patricia.Library.Business
{
    public class BinBusinessRepository : IBinBusinessRepository
    {
        private readonly IBinDataRepository binDataRepository;
        private readonly IApprovalDataRepository approvalDataRepository;
        private readonly IApprovalConfigDataRepository approvalConfiDataRepository;

        public BinBusinessRepository(
            IBinDataRepository binDataRepository,
            IApprovalDataRepository approvalDataRepository,
            IApprovalConfigDataRepository approvalConfiDataRepository

            )
        {
            this.approvalConfiDataRepository = approvalConfiDataRepository;
            this.approvalDataRepository = approvalDataRepository;
            this.binDataRepository = binDataRepository;
        }

        public List<BinDTO> RetrieveBins()
        {
            var bins = binDataRepository.RetrieveAll();

            return (from b in bins
                    select new BinDTO()
                    {
                        bin = b.Bin1,
                        id = b.Id,
                        scheme = b.Scheme,
                        createdOn = b.CreatedOn.ToString()

                    }).ToList();
        }

        public BaseDTO AddBin(BinDTO bin)
        {
            BaseDTO response = new BaseDTO();


            if (DoesBinExist(bin.bin.ToLower()))
            {
                response.responseCode = "01";

                response.responseMessage = "Bin Already Exists";

                return response;
            }
            else
            {
                if (bin.overrideApproval)
                {
                    var logForApproval = approvalConfiDataRepository.RetrieveByType(Enum.GetName(typeof(ApprovalType), ApprovalType.CreateBin)).Approve;

                    if (logForApproval == true)
                    {
                        Approval approvalDTO = new Approval()
                        {
                            Type = Enum.GetName(typeof(ApprovalType), ApprovalType.CreateBin),
                            RequestedBy = bin.username,
                            RequestedOn = DateTime.Now,
                            Details = JsonConvert.SerializeObject(bin),
                            ApprovalStatus = Enums.ApprovalStatus.PENDING.ToString()
                        };

                        if (approvalDataRepository.Save(approvalDTO))
                        {
                            response.responseCode = "00";
                            response.responseMessage = "Add Bin Request Has Been Logged Successfully for Approval";
                            return response;
                        }
                        else
                        {
                            response.responseCode = "01";
                            response.responseMessage = "Operation Failed";
                            return response;
                        }
                    }
                }

                Bin _bin = new Bin()
                {
                    Bin1 = bin.bin,
                    Scheme = bin.scheme,
                    CreatedOn = DateTime.Now
                };

                var isBinAdded = binDataRepository.Save(_bin);

                if (isBinAdded)
                {
                    response.responseCode = "00";
                    response.responseMessage = "Bin Has Been Added Successfully";
                    return response;
                }
                else
                {
                    response.responseCode = "01";
                    response.responseMessage = "Operation Failed";
                    return response;
                }
            }
            
        }

        public BaseDTO UpdateBin(BinDTO bin)
        {

            BaseDTO response = new BaseDTO();

            var oldPrintRequest = binDataRepository.GetBinById(bin.id);

            BinDTO oldBin = new BinDTO()
            {
                id = oldPrintRequest.Id,
                bin = oldPrintRequest.Bin1,
                scheme = oldPrintRequest.Scheme,
                createdOn = oldPrintRequest.CreatedOn.ToString()
            };

            if (DoesBinExist(bin.bin))
            {
                response.responseCode = "01";

                response.responseMessage = "Bin Already Exists";

                return response;
            }
            else
            {
                if (!bin.overrideApproval)
                {
                    var logForApproval = approvalConfiDataRepository.RetrieveByType(Enum.GetName(typeof(ApprovalType), ApprovalType.UpdateBin)).Approve;

                    if (logForApproval == true)
                    {
                        Approval approvalDTO = new Approval()
                        {
                            Type = Enum.GetName(typeof(ApprovalType), ApprovalType.UpdateBin),
                            RequestedBy = bin.username,
                            RequestedOn = DateTime.Now,
                            Details = JsonConvert.SerializeObject(bin),
                            OldDetails = JsonConvert.SerializeObject(oldBin),
                            ApprovalStatus = Enums.ApprovalStatus.PENDING.ToString()
                        };

                        if (approvalDataRepository.UpdateApproval(approvalDTO))
                        {
                            response.responseCode = "00";

                            response.responseMessage = "Update Bin Request Has Been Logged Successfully for Approval";

                            return response;
                        }
                        else
                        {
                            response.responseCode = "01";

                            response.responseMessage = "Operation Failed";

                            return response;
                        }
                    }

                    Bin _bin = new Bin()
                    {
                        Id = bin.id,
                        Bin1 = bin.bin,
                        Scheme = bin.scheme,
                        CreatedOn = DateTime.Now
                    };

                    var isBinAdded = binDataRepository.Update(_bin);

                    if (isBinAdded)
                    {
                        response.responseCode = "00";
                        response.responseMessage = "Bin Has Been Added Successfully";
                        return response;
                    }
                    else
                    {
                        response.responseCode = "01";
                        response.responseMessage = "Operation Failed";
                        return response;
                    }
                }
            }
            return null;
        }

        private bool DoesBinExist(string bin)
        {
            if (binDataRepository.Exists(bin))
            {
                return true;
            }
            return false;
        }




    }
}
