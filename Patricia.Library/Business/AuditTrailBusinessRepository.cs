﻿using Patricia.Library.BusinessContracts;
using Patricia.Library.DataContracts;
using Patricia.Library.DTO;
using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.Business
{
    public class AuditTrailBusinessRepository : IAuditTrailBusinessRespository
    {
        private readonly IAuditTrailDataRepository auditTrailDataRepository;

        public AuditTrailBusinessRepository(IAuditTrailDataRepository auditTrailDataRepository)
        {
            this.auditTrailDataRepository = auditTrailDataRepository;
        }

        public List<AuditDTO> RetrieveAll(string fromDate, string toDate)
        {
            var audits = auditTrailDataRepository.RetrieveAudits(fromDate, toDate);

            if (audits != null)
            {
                return MapAudits(audits);
            }

            return null;
        }

        private List<AuditDTO> MapAudits(List<AuditTrail> audits)
        {
            var auditDto = new List<AuditDTO>();

            if (audits.Any())
            {
                foreach (var audit in audits)
                {
                    var _auditDto = new AuditDTO()
                    {
                        id = audit.Id,
                        type = audit.Type,
                        requestedBy = audit.RequestedBy,
                        approvedBy = audit.ApprovedBy,
                        oldDetails = audit.OldDetails,
                        details = audit.Details,
                        requestedDate = audit.RequestedDate != null ? string.Format("{0:g}", audit.RequestedDate) : string.Empty,
                        approvalDate = audit.ApprovalDate != null ? string.Format("{0:g}", audit.ApprovalDate) : string.Empty
                    };
                    auditDto.Add(_auditDto);
                }
            }
            return auditDto;
        }
    }
}
