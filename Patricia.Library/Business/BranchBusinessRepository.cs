﻿using Newtonsoft.Json;
using Patricia.Library.BusinessContracts;
using Patricia.Library.Common;
using Patricia.Library.DataContracts;
using Patricia.Library.DTO;
using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Patricia.Library.Common.Enums;

namespace Patricia.Library.Business
{
    public class BranchBusinessRepository : IBranchBusinessRepository
    {
        private readonly IBranchDataRepository branchDataRepository;
        private readonly IApprovalDataRepository approvalDataRepository;
        private readonly IApprovalConfigDataRepository approvalConfiDataRepository;


        public BranchBusinessRepository(
           IBranchDataRepository branchDataRepository,
           IApprovalDataRepository approvalDataRepository,
           IApprovalConfigDataRepository approvalConfiDataRepository

           )
        {
            this.approvalConfiDataRepository = approvalConfiDataRepository;
            this.approvalDataRepository = approvalDataRepository;
            this.branchDataRepository = branchDataRepository;
        }

        public List<BranchDTO> RetrieveBranches()
        {
            var branches = branchDataRepository.RetrieveBranches();

            return (from b in branches
                    select new BranchDTO()
                    {
                        id = b.Id,
                        name = b.Name,
                        code = b.Code,
                        address = b.Address
                    }).ToList();
        }

        public BaseDTO AddBranch(BranchDTO branch)
        {
            BaseDTO response = new BaseDTO();


            if (branchDataRepository.Exists(branch.id, branch.name, branch.code))
            {
                response.responseCode = "01";

                response.responseMessage = $"Branch : {(branch.name)} Already Exists";

                return response;
            }
            else
            {
                if (!branch.overrideApproval)
                {
                    var logForApproval = approvalConfiDataRepository.RetrieveByType(Enum.GetName(typeof(ApprovalType), ApprovalType.CreateBranch)).Approve;

                    if (logForApproval == true)
                    {
                        Approval approvalDTO = new Approval()
                        {
                            Type = Enum.GetName(typeof(ApprovalType), ApprovalType.CreateBranch),
                            RequestedBy = branch.username,
                            RequestedOn = DateTime.Now,
                            Details = JsonConvert.SerializeObject(branch),
                            ApprovalStatus = Enums.ApprovalStatus.PENDING.ToString()
                        };

                        if (approvalDataRepository.Save(approvalDTO))
                        {
                            response.responseCode = "00";
                            response.responseMessage = "Add Branch Request Has Been Logged Successfully for Approval";
                            return response;
                        }
                        else
                        {
                            response.responseCode = "01";
                            response.responseMessage = "Operation Failed";
                            return response;
                        }
                    }
                }

                Branch _branch = new Branch()
                {
                    
                    Name = branch.name.ToLower(),
                    Code = branch.code,
                    Address = branch.address,
                };

                var isBranchCreated = branchDataRepository.Save(_branch);

                if (isBranchCreated)
                {
                    response.responseCode = "00";
                    response.responseMessage = $"Branch : {(_branch.Name)} has been Created Successfully";
                    return response;
                }
                else
                {
                    response.responseCode = "01";
                    response.responseMessage = "Operation Failed";
                    return response;
                }
            }
            
        }

        public BaseDTO UpdateBranch(BranchDTO branch)
        {

            BaseDTO response = new BaseDTO();

            var existingBranch = branchDataRepository.RetrieveBranchByID(branch.id);

            BranchDTO oldBranch = new BranchDTO()
            {
                id = existingBranch.Id,
                name = existingBranch.Name,
                address = existingBranch.Address,
                code = existingBranch.Code
            };

            if (!branchDataRepository.Exists(branch.id, branch.name, branch.code))
            {
                response.responseCode = "01";

                response.responseMessage = $"Branch : {(branch.name)} does not Exist";

                return response;
            }
            else
            {
                if (!branch.overrideApproval)
                {
                    var logForApproval = approvalConfiDataRepository.RetrieveByType(Enum.GetName(typeof(ApprovalType), ApprovalType.UpdateBranch)).Approve;

                    if (logForApproval == true)
                    {
                        Approval approvalDTO = new Approval()
                        {
                            Type = Enum.GetName(typeof(ApprovalType), ApprovalType.UpdateBranch),
                            RequestedBy = branch.username,
                            RequestedOn = DateTime.Now,
                            Details = JsonConvert.SerializeObject(branch),
                            OldDetails = JsonConvert.SerializeObject(oldBranch),
                            ApprovalStatus = Enums.ApprovalStatus.PENDING.ToString()
                        };

                        if (approvalDataRepository.Save(approvalDTO))
                        {
                            response.responseCode = "00";

                            response.responseMessage = "Update Branch Request Has Been Logged Successfully for Approval";

                            return response;
                        }
                        else
                        {
                            response.responseCode = "01";

                            response.responseMessage = "Operation Failed";
                        }
                    }
                }

                Branch _branch = new Branch()
                {
                    Id = branch.id,
                    Name = branch.name.ToLower(),
                    Code = branch.code,
                    Address = branch.address,
                };

                var isBranchCreated = branchDataRepository.Update(_branch);

                if (isBranchCreated)
                {
                    response.responseCode = "00";
                    response.responseMessage = $"Branch : {(_branch.Name)} has been updated Successfully";
                    return response;
                }
                else
                {
                    response.responseCode = "01";
                    response.responseMessage = "Operation Failed";
                    return response;
                }

            }
            return null;
        }
    }
}
