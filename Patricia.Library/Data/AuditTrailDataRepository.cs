﻿using Patricia.Library.DataContracts;
using Patricia.Library.Extensions;
using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Patricia.Library.Data
{
    public class AuditTrailDataRepository : IAuditTrailDataRepository
    {
        private readonly PatriciaCardEntities dataContext;

        public AuditTrailDataRepository(PatriciaCardEntities dataContext)
        {
            this.dataContext = dataContext;
        }

        public bool Save(AuditTrail auditTrail)
        {
            try
            {
                dataContext.AuditTrails.Add(auditTrail);
                dataContext.SaveChanges();
                return true;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            
        }

        public List<AuditTrail> RetrieveAudits(string fromDate, string toDate)
        {
            var _fromDate = fromDate.ToDate();
            var _toDate = toDate.ToDate();

            var audits = dataContext.AuditTrails.AsNoTracking()
                                    .Where(a => a.RequestedDate >= _fromDate &&
                                                a.RequestedDate <= _toDate)
                                                .OrderByDescending(a => a.Id)
                                                .ToList();
            return audits;
        }
    }
}
