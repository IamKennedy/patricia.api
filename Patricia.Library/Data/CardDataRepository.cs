﻿

using Patricia.Library.Common;
using Patricia.Library.DataContracts;
using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Patricia.Library.Data
{
    public class CardDataRepository : ICardDataRepository
    {
        private readonly PatriciaCardEntities dataContext;

        public CardDataRepository(PatriciaCardEntities dataContext)
        {
            this.dataContext = dataContext;
        }

        public Card GetCardByPan(string cardPan)
        {
           var cardInfo = dataContext.Cards.AsNoTracking().FirstOrDefault(c => c.CardNumber == cardPan && c.CardStatus == Enums.CardStatus.ACTIVE.ToString());

            if (cardInfo == null)
            {
                return null;
            }
            else
            {
                return cardInfo;
            }
        }

        public Card GetCardByAccountNumber(string accountNumber)
        {
            var cardInfo = dataContext.Cards.AsNoTracking().FirstOrDefault(c => c.AccountNumber == accountNumber);

            if (cardInfo == null)
            {
                return null;
            }
            else
            {
                return cardInfo;
            }
        }

        public Card GetActiveCardByAccountNumber(string accountNumber)
        {
            var cardInfo = dataContext.Cards.AsNoTracking().FirstOrDefault(c => c.AccountNumber == accountNumber && c.CardStatus == Enums.CardStatus.ACTIVE.ToString());

            if (cardInfo == null)
            {
                return null;
            }
            else
            {
                return cardInfo;
            }
        }

        public Card GetCardbyID(long id)
        {
            return dataContext.Cards.AsNoTracking().FirstOrDefault(c => c.Id == id);
        }

        public List<Card> GetActiveCards()
        {
             return dataContext.Cards.AsNoTracking().Where(c => c.CardStatus == Enums.CardStatus.ACTIVE.ToString()).ToList();
        }

        public bool Block(string cardPan)
        {
            var customerCard = dataContext.Cards.FirstOrDefault(c => c.CardNumber == cardPan);

            if (customerCard.AccountNumber != null)
            {
                customerCard.CardStatus = Enums.CardStatus.BLOCKED.ToString();
                dataContext.Entry(customerCard).State = EntityState.Modified;
                dataContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }

           
        }

        public bool Unblock(string cardPan)
        {
            var customerCard = dataContext.Cards.FirstOrDefault(c => c.CardNumber == cardPan);

            if (customerCard != null)
            {
                customerCard.CardStatus = Enums.CardStatus.ACTIVE.ToString();
                dataContext.Entry(customerCard).State = EntityState.Modified;
                dataContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
           
        }

        public bool Save(Card card)
        {
            dataContext.Cards.Add(card);
            dataContext.SaveChanges();
            return true;
        }
    }
}
