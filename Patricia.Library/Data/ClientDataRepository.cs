﻿using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.Data
{
    public class ClientDataRepository : IDisposable
    {
        private readonly PatriciaCardEntities dataContext = new PatriciaCardEntities();


        public PatriciaClient ValidateClient(string ClientID, string ClientSecret)
        {
            return dataContext.PatriciaClients.FirstOrDefault(user =>
             user.ClientID == ClientID
            && user.ClientSecret == ClientSecret);
        }

        public void Dispose()
        {
            dataContext.Dispose();
        }
    }
}
