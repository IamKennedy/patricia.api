﻿
using Patricia.Library.Common;
using Patricia.Library.DataContracts;
using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;


namespace Patricia.Library.Data
{
    public class UserDataRepository : IUserDataRepository
    {
        private readonly PatriciaCardEntities dataContext;

        public UserDataRepository(PatriciaCardEntities dataContext)
        {
            this.dataContext = dataContext;
        }

        public UserDataRepository()
        {
        }

        public User ValidateUser(string username, string password)
        {
            var _user = dataContext.Users.AsNoTracking().FirstOrDefault(user => user.Username.Equals(username, StringComparison.OrdinalIgnoreCase) && user.HashedPassword == password && user.UserStatus == Enums.UserStatus.ACTIVE.ToString());

            return _user;
        }

        public User ValidateUserWithoutPassword(string username)
        {
            var _user = dataContext.Users.AsNoTracking().FirstOrDefault(user => user.Username.Equals(username, StringComparison.OrdinalIgnoreCase));

            if (_user != null)
            {
                return _user;
            }

            return _user;
        }

        public static User ValidateToken(string username, string password)
        {
            PatriciaCardEntities dataContext = new PatriciaCardEntities();

            var _user = dataContext.Users.AsNoTracking().FirstOrDefault(user => user.Username.Equals(username, StringComparison.OrdinalIgnoreCase) && user.HashedPassword == password && user.UserStatus == Enums.UserStatus.ACTIVE.ToString());

            return _user;
        }
        public User RetriveUserByUsername(string username)
        {
            return dataContext.Users.AsNoTracking().FirstOrDefault(user => user.Username.Equals(username, StringComparison.OrdinalIgnoreCase));
        }
        public User RetriveUserByUserID(long userID)
        {
            return dataContext.Users.AsNoTracking().FirstOrDefault(user => user.Id == userID);
        }
        public List<User> GetUsers()
        {
            return dataContext.Users.AsNoTracking().ToList();
        }
        public bool DisableUser(int userId)
        {
            var user = dataContext.Users.FirstOrDefault(U => U.Id == userId);

            if (user != null)
            {
                user.UserStatus = Enums.UserStatus.IN_ACTIVE.ToString();
            }

            dataContext.Entry(user).State = EntityState.Modified;
            dataContext.SaveChanges();
            return true;
        }
        public bool EnableUser(int userId)
        {
            var user = dataContext.Users.FirstOrDefault(U => U.Id == userId);

            if (user != null)
            {
                user.UserStatus = Enums.UserStatus.ACTIVE.ToString();
            }

            dataContext.Entry(user).State = EntityState.Modified;
            dataContext.SaveChanges();
            return true;
        }
        public bool ChangePassword(long userId, string OldPassword, string newPassword)
        {
            var user = dataContext.Users.FirstOrDefault(U => U.Id == userId && U.HashedPassword == CryptoLib.SHA512(OldPassword));

            if (user != null)
            {
                user.HashedPassword = CryptoLib.SHA512(newPassword);
                user.FirstTimeLogon = false;
                dataContext.Entry(user).State = EntityState.Modified;
                dataContext.SaveChanges();
                return true;
            }

            return false;
            
        }
        public bool Exists(string username)
        {
            var existingUser = dataContext.Users
                                .Where(t => t.Username == username)
                                .FirstOrDefault();

            if (existingUser == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public static bool Uservalidate(string username, string password)
        {
            PatriciaCardEntities dataContext = new PatriciaCardEntities();

            var existingUser = dataContext.Users
                               .Where(t => t.Username.Equals(username, StringComparison.OrdinalIgnoreCase))
                               .FirstOrDefault();

            if ((existingUser != null))
            {
                if(existingUser.HashedPassword == password)
                    return true;
            }
            else
            {
                return false;
            }

            return false;
        }
        public bool Update(User user)
        {
            var existingUser = dataContext.Users
                                            .Where(t => t.Id == user.Id)
                                            .FirstOrDefault();

            existingUser.Email = user.Email;        
            existingUser.LastName = user.LastName;
            existingUser.OtherNames = user.OtherNames;
            existingUser.Role = user.Role;
            existingUser.UserStatus = user.UserStatus;
            existingUser.HashedPassword = user.HashedPassword;
            

            dataContext.Entry(existingUser).State = EntityState.Modified;
            dataContext.SaveChanges();
            return true;


        }
        public bool Save(User user)
        {
            user.FirstTimeLogon = true;
            dataContext.Users.Add(user);
            dataContext.SaveChanges();
            return true;
        }

       
    }
}
