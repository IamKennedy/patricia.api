﻿using Patricia.Library.DataContracts;
using Patricia.Library.Extensions;
using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.Data
{
    public class ApprovalDataRepository : IApprovalDataRepository
    {
        private readonly PatriciaCardEntities dataContext;

        public ApprovalDataRepository(PatriciaCardEntities dataContext)
        {
            this.dataContext = dataContext;
        }

        public bool Save(Approval approval)
        {
            dataContext.Approvals.Add(approval);
            dataContext.SaveChanges();
            return true;
        }
        public List<Approval> RetrieveApprovals()
        {
            var approvals = dataContext.Approvals.AsNoTracking()
                .OrderByDescending(a => a.Id)
                .ToList();
            return approvals;
        }
        public bool UpdateApproval(Approval approval)
        {
            var existingApproval = dataContext.Approvals
                                            .Where(a => a.Id == approval.Id)
                                            .FirstOrDefault();
            existingApproval.ApprovalStatus = approval.ApprovalStatus;
            existingApproval.ApprovedBy = approval.ApprovedBy;
            existingApproval.ApprovedOn = approval.ApprovedOn;

            dataContext.Entry(existingApproval).State = EntityState.Modified;
            dataContext.SaveChanges();
            return true;
        }
        public List<Approval> RetrieveFilteredApprovals(string fromDate, string toDate, string status, string approvalType)
        {
            var _fromDate = fromDate.ToDate();
            var _toDate = toDate.ToDate();

            var approvals = dataContext.Approvals.AsNoTracking()
                             .Where(a => a.ApprovalStatus == status &&
                                         a.RequestedOn >= _fromDate &&
                                         a.RequestedOn <= _toDate &&
                                         a.ApprovalStatus == status ||
                                         a.Type == approvalType)
                                         .ToList(); 
            return approvals;
        }
        public Approval RetrieveById(long Id)
        {
            var approval = dataContext.Approvals.AsNoTracking()
                                  .Where(a => a.Id == Id)
                                  .FirstOrDefault();
            return approval;
        }

        public Approval RetrieveByUsername(string username)
        {
            var approval = dataContext.Approvals.AsNoTracking()
                                  .Where(a => a.RequestedBy == username)
                                  .FirstOrDefault();
            return approval;
        }




    }
}
