﻿using Patricia.Library.DataContracts;
using Patricia.Library.Extensions;
using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.Data
{
    public class CardPRDataRepository : ICardPRDataRepository
    {
        private readonly PatriciaCardEntities dataContext;

        public CardPRDataRepository(PatriciaCardEntities dataContext)
        {
            this.dataContext = dataContext;
        }

        public List<CardPrintRequest> RetrievePrintRequests()
        {
            var existingRequests = dataContext.CardPrintRequests.AsNoTracking()
                                      .OrderByDescending(c => c.Id)
                                      .ToList();

            return existingRequests;
        }

        public bool Save(CardPrintRequest printRequest)
        {
            dataContext.CardPrintRequests.Add(printRequest);
            dataContext.SaveChanges();
            return true;
        }

        public bool UpdatePrintRequest(CardPrintRequest newprintRequest, CardPrintRequest oldprintRequest)
        {
            var existingRequest = dataContext.CardPrintRequests
                                            .Where(a => a.Id == oldprintRequest.Id)
                                            .FirstOrDefault();
            if (existingRequest != null)
            {
                existingRequest.Status = newprintRequest.Status;
                existingRequest.ApprovedBy = newprintRequest.ApprovedBy;
                existingRequest.DateApproved = newprintRequest.DateApproved;
            }

            dataContext.Entry(existingRequest).State = EntityState.Modified;
            dataContext.SaveChanges();
            return true;
        }

        public CardPrintRequest GetPrintRequestbyId(long requestId)
        {
            var printRequest = dataContext.CardPrintRequests.AsNoTracking()
                                      .Where(a => a.Id == requestId).FirstOrDefault();

            if (printRequest != null)
            {
                return printRequest;
            }

            return null;
        }

        public List<CardPrintRequest> RetrieveFilteredRequests(string fromDate, string toDate, string status, string branch)
        {
            var _toDate = toDate.ToDate();
            var _fromDate = fromDate.ToDate();

            var requests = dataContext.CardPrintRequests.AsNoTracking()
                            .Where(a => a.Status == status &&
                                        a.DateRequested >= _fromDate &&
                                        a.DateRequested <= _toDate)
                                        .ToList();
            return requests;
        }
    }
}
