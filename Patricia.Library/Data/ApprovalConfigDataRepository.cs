﻿using Patricia.Library.DataContracts;
using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.Data
{
    public class ApprovalConfigDataRepository : IApprovalConfigDataRepository
    {
        public List<ApprovalConfiguration> RetrieveAll()
        {
            using (var dataContext = new PatriciaCardEntities())
            {
                var configs = dataContext.ApprovalConfigurations.ToList();

                return configs;
            }      
        }

        public ApprovalConfiguration RetrieveByType(string type)
        {
            using (var dataContext = new PatriciaCardEntities())
            {
                var configs = dataContext.ApprovalConfigurations.Where(f => f.Type == type).FirstOrDefault();
                return configs;
            }
        }


        public bool Update(List<ApprovalConfiguration> configs)
        {
            using (var dataContext = new PatriciaCardEntities())
            {
                configs.ForEach(config =>
                {
                    var existingConfig = dataContext.ApprovalConfigurations
                                          .Where(c => c.Id == config.Id)
                                          .FirstOrDefault();

                    if (existingConfig != null)
                    {
                        existingConfig.Approve = config.Approve;
                        dataContext.Entry(existingConfig).State = EntityState.Modified;
                        dataContext.SaveChanges();
                    }

                });
            }
            return true;
        }
    }
}
