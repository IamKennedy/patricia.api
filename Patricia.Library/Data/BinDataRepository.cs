﻿using Patricia.Library.DataContracts;
using Patricia.Library.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Patricia.Library.Data
{
    public class BinDataRepository : IBinDataRepository
    {
        private readonly PatriciaCardEntities dataContext;

        public BinDataRepository(PatriciaCardEntities dataContext)
        {
            this.dataContext = dataContext;
        }

        public bool Exists(string bin)
        {
            var existingBin = dataContext.Bins
                                .Where(t => t.Bin1 == bin)
                                .FirstOrDefault();

            if (existingBin == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public List<Bin> RetrieveAll()
        {
            var bins = dataContext.Bins.AsNoTracking().ToList();
            return bins;
        }

        public bool Update(Bin bin)
        {
            var existingBin = dataContext.Bins
                                            .Where(t => t.Id == bin.Id)
                                            .FirstOrDefault();
            existingBin.Bin1 = bin.Bin1;
            existingBin.Scheme = bin.Scheme;
            existingBin.CreatedOn = bin.CreatedOn;
            dataContext.Entry(existingBin).State = EntityState.Modified;
            dataContext.SaveChanges();
            return true;
        }

        public Bin GetBinById(long Id)
        {
            var bin = dataContext.Bins.AsNoTracking().Where(b => b.Id == Id).FirstOrDefault();
            return bin;
        }

        public bool Save(Bin bin)
        {
            dataContext.Bins.Add(bin);
            dataContext.SaveChanges();
            return true;
        }
    }
}
