﻿using Patricia.Library.DataContracts;
using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.Data
{
    public class IPDataRepository : IIPDataRepository
    {

        private readonly PatriciaCardEntities dataContext;

        public IPDataRepository(PatriciaCardEntities dataContext)
        {
            this.dataContext = dataContext;
        }

        public bool Exists(string ip)
        {
            using (var context = new PatriciaCardEntities())
            {
                var IP = context.IPS.AsNoTracking()
                                .Where(t => t.IPAddress == ip && t.IPStatus == true);
                                       

                if (IP.Any())
                {
                    return true;

                }
                else
                {
                    return false;
                }
            }
        }

        public List<IP> RetrieveIPS()
        {
            using (var context = new PatriciaCardEntities())
            {
                var ip = context.IPS.AsNoTracking().ToList();
                return ip;
            }
        }

        public IP RetrieveIPByID(long ipID)
        {
            using (var context = new PatriciaCardEntities())
            {
                var ip = context.IPS.AsNoTracking().Where(f => f.Id == ipID).FirstOrDefault();

                return ip;
            }
        }

        public List<IP> RetrieveIPs()
        {
            using (var context = new PatriciaCardEntities())
            {
                var ips = context.IPS.ToList();
                return ips;
            }
        }

        public bool Save(IP ip)
        {
            using (var context = new PatriciaCardEntities())
            {
                context.IPS.Add(ip);
                context.SaveChanges();
                return true;
            }
        }

        public bool Update(IP ip)
        {
            using (var context = new PatriciaCardEntities())
            {
                var existingIP = context.IPS
                                .Where(t => t.Id == ip.Id)
                                .FirstOrDefault();

                existingIP.Name = ip.Name;
                existingIP.IPAddress = ip.IPAddress;
                existingIP.IPStatus = ip.IPStatus;
                existingIP.Description = ip.Description;

                context.Entry(existingIP).State = EntityState.Modified;
                context.SaveChanges();
                return true;
            }
        }
    }
}

