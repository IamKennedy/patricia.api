﻿using Patricia.Library.DataContracts;
using Patricia.Library.Extensions;
using Patricia.Library.Models;
using System.Collections.Generic;
using System.Linq;

namespace Patricia.Library.Data
{
    public class TransactionDataRepository : ITransactionDataRepository
    {
        private readonly PatriciaCardEntities dataContext;

        public TransactionDataRepository(PatriciaCardEntities dataContext)
        {
            this.dataContext = dataContext;
        }

        public List<Transaction> RetrieveTransactions(string fromDateStr, string toDateStr)
        {
            var fromDate = fromDateStr.ToDate();
            var toDate = toDateStr.ToDate(addOneDay: true);

            var transactions = dataContext.Transactions.AsNoTracking()
                    .Where(t =>
                        t.TransactionDate >= fromDate &&
                        t.TransactionDate <= toDate)
                    .OrderByDescending(t => t.Id)
                    .ToList();
            return transactions;
        }

        public List<Transaction> RetrieveTransactionsByAccountNumber(string accountNumber)
        {
            var transactions = dataContext.Transactions.AsNoTracking()
                    .Where(t => t.AccountNumber == accountNumber)
                    .OrderByDescending(t => t.Id)
                    .ToList();
            return transactions;
        }

        public List<Transaction> RetrieveTransactionsByCardNumber(string hashedpan)
        {
            var transactions = dataContext.Transactions.AsNoTracking()
                    .Where(t => t.CardNumber == hashedpan)
                    .OrderByDescending(t => t.Id)
                    .ToList();
            return transactions;
        }

        public Transaction RetrieveTransactionsByReferenceCode(string referenceID)
        {
            var transactions = dataContext.Transactions.AsNoTracking().FirstOrDefault(r => r.Reference == referenceID);
                    
            return transactions;
        }

        public bool Save(Transaction transaction)
        {
            dataContext.Transactions.Add(transaction);
            dataContext.SaveChanges();
            return true;
        }
    }
}
