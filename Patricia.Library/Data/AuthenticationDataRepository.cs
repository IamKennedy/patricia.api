﻿using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.Data
{
    public class AuthenticationDataRepository
    {
        private readonly PatriciaCardEntities dataContext = new PatriciaCardEntities();

        public async Task<bool> AddRefreshToken(RefreshToken token)
        {
            var existingToken = dataContext.RefreshTokens.FirstOrDefault(r => r.UserName == token.UserName && r.ClientID == token.ClientID);

            if (existingToken != null)
            {
                var result = await RemoveRefreshToken(existingToken);
            }
            dataContext.RefreshTokens.Add(token);

            return await dataContext.SaveChangesAsync() > 0;
        }

        public async Task<bool> RemoveRefreshTokenByID(string refreshTokenId)
        {
            var refreshToken = await dataContext.RefreshTokens.FindAsync(refreshTokenId);

            if (refreshToken != null)
            {
                dataContext.RefreshTokens.Remove(refreshToken);
                return await dataContext.SaveChangesAsync() > 0;
            }
            return false;
        }

        public async Task<bool> RemoveRefreshToken(RefreshToken refreshToken)
        {
            dataContext.RefreshTokens.Remove(refreshToken);
            return await dataContext.SaveChangesAsync() > 0;
        }

        public async Task<RefreshToken> FindRefreshToken(string refreshTokenId)
        {
            var refreshToken = await dataContext.RefreshTokens.FindAsync(refreshTokenId);
            return refreshToken;
        }

        public List<RefreshToken> GetAllRefreshTokens()
        {
            return dataContext.RefreshTokens.ToList();
        }

        public void Dispose()
        {
            dataContext.Dispose();
        }
    }
}
