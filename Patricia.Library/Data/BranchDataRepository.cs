﻿using Patricia.Library.DataContracts;
using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.Data
{
    public class BranchDataRepository : IBranchDataRepository
    {
        private readonly PatriciaCardEntities dataContext;

        public BranchDataRepository(PatriciaCardEntities dataContext)
        {
            this.dataContext = dataContext;
        }

        public bool Exists(long id, string name, string code)
        {
            using (var context = new PatriciaCardEntities())
            {
                var existingBranch = context.Branches.AsNoTracking()
                                .Where(t => t.Name == name || t.Code == code);

                if (existingBranch.Any())
                {
                    var br = existingBranch.FirstOrDefault();

                    if (br.Id == id)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public List<Branch> RetrieveActiveBranches()
        {
            using (var context = new PatriciaCardEntities())
            {
                var branches = context.Branches.AsNoTracking().ToList();
                return branches;
            }
        }

        public Branch RetrieveBranchByID(long branchID)
        {
            using (var context = new PatriciaCardEntities())
            {
                var branch = context.Branches.AsNoTracking().Where(f => f.Id == branchID).FirstOrDefault();

                return branch;
            }
        }

        public List<Branch> RetrieveBranches()
        {
            using (var context = new PatriciaCardEntities())
            {
                var branches = context.Branches.AsNoTracking().ToList();
                return branches;
            }
        }

        public bool Save(Branch branch)
        {
            using (var context = new PatriciaCardEntities())
            {
                context.Branches.Add(branch);
                context.SaveChanges();
                return true;
            }
        }

        public bool Exists(string branch)
        {
            var existingBin = dataContext.Branches.AsNoTracking()
                                .Where(t => t.Name == branch)
                                .FirstOrDefault();

            if (existingBin == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool Update(Branch branch)
        {
            using (var context = new PatriciaCardEntities())
            {
                var existingBranch = context.Branches
                                .Where(t => t.Id == branch.Id)
                                .FirstOrDefault();

                existingBranch.Name = branch.Name;
                existingBranch.Code = branch.Code;
                existingBranch.Address = branch.Address;


                context.Entry(existingBranch).State = EntityState.Modified;
                context.SaveChanges();
                return true;
            }
        }
    }
}
