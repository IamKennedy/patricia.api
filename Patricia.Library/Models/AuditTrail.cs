//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Patricia.Library.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class AuditTrail
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public string ClientIP { get; set; }
        public string Details { get; set; }
        public string OldDetails { get; set; }
        public string RequestedBy { get; set; }
        public Nullable<System.DateTime> RequestedDate { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<System.DateTime> ApprovalDate { get; set; }
    }
}
