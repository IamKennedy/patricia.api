﻿using Patricia.Library.Common;
using System;


namespace Patricia.Library.CommonContracts
{ 
    public interface IErrorLogger
    {
        void WriteError(Exception error, Enums.LogType logType);

        string GetMessage(Exception ex);
    }
}
