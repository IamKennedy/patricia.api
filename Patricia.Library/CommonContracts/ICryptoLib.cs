﻿

namespace Patricia.Library.CommonContracts
{
    public interface ICryptoLib 
    {
        string Encrypt(string key, string data);

        string Decrypt(string key, string data);
    }
}
