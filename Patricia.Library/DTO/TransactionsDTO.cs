﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.DTO
{
    public class TransactionsDTO
    {
        public string reference { get; set; }
        public string accountNumber { get; set; }
        public string serviceNarration { get; set; }
        public decimal? amount { get; set; }
        public string transactionDate { get; set; }
        public string transactionStatus { get; set; }
        public string statusCode { get; set; }
        public string description { get; set; }
    }
}
