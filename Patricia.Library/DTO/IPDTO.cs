﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.DTO
{
    public class IPDTO : BaseDTO
    {
        public long id { get; set; }
        public string name { get; set; }
        public string ipAddress { get; set; }
        public string ipStatus { get; set; }
        public string description { get; set; }
        public string details { get; set; }
        public string oldDetails { get; set; }
        public bool overrideApproval { get; set; }
        public string operatingUser { get; set; }
        public string approvedBy { get; set; }
    }
}
