﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.DTO
{
    public class BinDTO : BaseDTO
    {
        public long id { get; set; }
        public string scheme { get; set; }
        public string bin { get; set; }
        public string details { get; set; }
        public string createdOn { get; set; }
        public string oldDetails { get; set; }
        public bool overrideApproval { get; set; }
        public string username { get; set; }
    }
}
