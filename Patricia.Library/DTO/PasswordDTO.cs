﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.DTO
{
    public class PasswordDTO
    {
        public string username { get; set; }
        public string oldpassword { get; set; }
        public string newPassword { get; set; }
    }
}
