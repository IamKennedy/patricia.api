﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.DTO
{
    public class PanDTO
    {
        public string cardNumber { get; set; }
        public string requestedBy { get; set; }
        public string approvedBy { get; set; }
        public string accountNumber { get; set; }
        public bool isBlockCardConfiguredForApproval { get; set; }
        public bool isUnBlockCardConfiguredForApproval { get; set; }
    }
}
