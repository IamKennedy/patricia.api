﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.DTO
{
    public class CardDTO : BaseDTO
    {
        public long Id { get; set; }
        public string cardNumber { get; set; }
        public string accountNumber { get; set; }
        public string accountName { get; set; }
        public string cardStatus { get; set; }
        public string cardExpiryDate { get; set; }
        public string requestedBy { get; set; }
        public string approvedBy { get; set; }
        public bool isLinkCardConfiguredForApproval { get; set; }
    }
}
