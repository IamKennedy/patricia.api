﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.DTO
{
    public class BranchDTO : BaseDTO
    {
        public long id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string address { get; set; }
        public string details { get; set; }
        public string oldDetails { get; set; }
        public bool overrideApproval { get; set; }
        public string username { get; set; }
    }
}
