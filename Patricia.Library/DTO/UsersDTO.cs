﻿using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.DTO
{
    public class UsersDTO : BaseDTO
    {
        public long id { get; set; }

        public string firstName { get; set; }

        public string lastName { get; set; }

        public string clientIP { get; set; }

        public string branch { get; set; }

        public string userExt { get; set; }

        public string username { get; set; }

        public string hashedPassword { get; set; }

        public string oldPassword { get; set; }

        public string newPassword { get; set; }

        public string password { get; set; }

        public string role { get; set; }

        public string email { get; set; }

        public string userStatus { get; set; }

        public bool? firstTimeLogon { get; set; }

        public string details { get; set; }

        public string oldDetails { get; set; }

        public bool overrideApproval { get; set; }

        public string operationUsername { get; set; }

        public string approvedBy { get; set; }

        public string dateRequested { get; set; }

        public string dateApproved { get; set; }
    }
}
