﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.DTO
{
    public class AuditDTO
    {
        public long id { get; set; }
        public string type { get; set; }
        public string details { get; set; }
        public string oldDetails { get; set; }
        public string requestedBy { get; set; }
        public string requestedDate { get; set; }
        public string approvedBy { get; set; }
        public string approvalDate { get; set; }
    }
}
