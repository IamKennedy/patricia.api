﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.DTO
{
    public class ApprovalDTO : BaseDTO
    {
        public long Id { get; set; }
        public string status { get; set; }
        public string details { get; set; }
        public string obj { get; set; }
        public string oldDetails { get; set; }
        public string type { get; set; }
        public string requestedBy { get; set; }
        public string approvedBy { get; set; }
        public string requestedOn { get; set; }
        public string approvedOn { get; set; }

    }
    
}
