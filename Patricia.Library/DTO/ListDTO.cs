﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.DTO
{
    public class ListDTO<T> : BaseDTO
    {
        public List<T> data { get; set; }
    }
}
