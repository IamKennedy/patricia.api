﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Patricia.Library.Common.Enums;

namespace Patricia.Library.DTO
{
    public class CardPRDTO : BaseDTO
    {
        public long id { get; set; }
        public string pan { get; set; }
        public string accountNumber { get; set; }
        public string printStatus { get; set; }     
        public string cardStatus { get; set; }
        public string requestedBy { get; set; }
        public string requestedDate { get; set; }
        public bool? isMobile { get; set; }
        public bool? isWeb { get; set; }
        public string approvalDate { get; set; }
        public string approvedBy { get; set; }
        public string nameToPrint { get; set; }
        public string printedOn{ get; set; }
        public bool overrideApproval { get; set; }
    }
}
