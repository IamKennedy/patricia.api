﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.DTO
{
    public class BaseDTO
    {
        public string responseCode { get; set; }

        public string responseMessage { get; set; }
    }
}
