﻿using Patricia.Library.CommonContracts;
using Patricia.Library.Extensions;
using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;


namespace Patricia.Library.Common
{
    public class ErrorLogger : IErrorLogger
    {
        private readonly ReaderWriterLockSlim @lock = new ReaderWriterLockSlim();

        public void WriteError(Exception error, Enums.LogType logType)
        {
            var directoryPath = System.Configuration.ConfigurationManager.AppSettings.Get("LogFilePath");

            var filename =
                $"errorLog_{DateTime.Now.Day.ToString()}{DateTime.Now.Month.ToString()}{DateTime.Now.Year.ToString()}";

            var filepath = directoryPath + "\\" + filename + ".txt";

            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            var errMsg = GetMessage(error);
            var stackTrace = error.ToLogString(Environment.StackTrace);

            @lock.EnterWriteLock();

            try
            {
                using (var file = new FileStream(filepath, FileMode.Append, FileAccess.Write, FileShare.Read))
                using (var writer = new StreamWriter(file, Encoding.Unicode))
                {
                    writer.WriteLine("[Time Stamp: " + DateTime.Now.ToString(CultureInfo.InvariantCulture) + "]\n");
                    writer.WriteLine($"[{logType.ToString()}:]\n");
                    writer.WriteLine(errMsg + "\n");
                    writer.WriteLine(stackTrace + "\n\n");
                    writer.WriteLine("");
                }
            }
            finally
            {
                @lock.ExitWriteLock();
            }


        }

        public string GetMessage(Exception ex)
        {
            var errorMessage = string.Empty;
            errorMessage += $"{ex.Message} ";
            if (ex.InnerException != null)
            {
                errorMessage += GetMessage(ex.InnerException);
            }
            return errorMessage;
        }
    }
}
