﻿

using System;
using System.ComponentModel;
using System.Reflection;

namespace Patricia.Library.Common
{
    public class Enums
    {
        public enum CardStatus
        {          
            ACTIVE,
            IN_ACTIVE,
            BLOCKED,
        }

        public enum MapStatus
        {
            MAPPED, 
            UNMAPPED
        }

        public enum LogType
        {
            Info,
            Error
        }

        public enum PrintRequestStatus
        {
            PENDING,
            SUCCESSFUL,
            FAILED,

        }

        public enum RoleType
        {
            SuperAdmin,
            Admin,
            StandardUser
        }

        public enum UserStatus
        {
            ACTIVE,
            IN_ACTIVE
        }

        public enum ApprovalStatus
        {
            PENDING, 
            APPROVED,
            DECLINED
        }

        public enum TableNames
        {
            SYSTEMAPPROVALS,
            SYSTEMAUDITTRAIL,
            CARDACCOUNTREQUESTS
        }

        public enum ApprovalType
        {
            [Description("User Login")]
            UserLogin,
            [Description("Print Utility User Login")]
            PrintUtilityUserLogin,
            [Description("Create Branch")]
            CreateBranch,
            [Description("Update Branch")]
            UpdateBranch,
            [Description("Create User")]
            CreateUser,
            [Description("Update User")]
            UpdateUser,
            [Description("Create Role")]
            CreateRole,
            [Description("Update Role")]
            UpdateRole,
            [Description("Create Allowed IP")]
            CreateIP,
            [Description("Update Allowed IP")]
            UpdateIP,
            [Description("Delete Allowed IP")]
            DeleteIP,
            [Description("Create Bin")]
            CreateBin,
            [Description("Update Bin")]
            UpdateBin,
            [Description("Reset Card Print Status")]
            ResetCardPrintStatus,
            [Description("Inserted Print Records")]
            InsertedPrintRecords,
            [Description("Create Function")]
            CreateFunction,
            [Description("Update Function")]
            UpdateFunction,
            [Description("Reset Password")]
            ResetPassword


        }

        public static string GetDescription(Enum en)
        {
            Type type = en.GetType();

            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attrs != null && attrs.Length > 0)
                {
                    return ((DescriptionAttribute)attrs[0]).Description;
                }
            }

            return en.ToString();
        }
    }
}
