﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Patricia.Library.Security
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class AuthorizeRequestAttribute : AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            var env = ConfigurationManager.AppSettings.Get("ENV");

            if (env == "PROD")
            {
                return base.IsAuthorized(actionContext);
            }
            else
            {
                return true;
            }
        }
    }
}
