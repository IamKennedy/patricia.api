﻿
using Patricia.Library.BusinessContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Patricia.Library.Attributes
{
    public class AuthorizeAccessAttribute : AuthorizationFilterAttribute
    {
        private string [] allowedRoles;

        public AuthorizeAccessAttribute(
            string []  allowedRoles)
        {
            this.allowedRoles = allowedRoles;
        }


        //public override void OnAuthorization(HttpActionContext actionContext)
        //{
        //    try
        //    {
        //        IEnumerable<string> headerValues;

        //        var userId = string.Empty;

        //        if (actionContext.Request.Headers.TryGetValues("X-Initiator-Type", out headerValues))
        //        {
        //            userId = headerValues.FirstOrDefault();
        //        }

        //        if (string.IsNullOrEmpty(userId))
        //        {
        //            actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, $"Unauthorized access to resource! {actionContext.Request.RequestUri.AbsolutePath}");
        //            return;
        //        }
        //        else
        //        {
        //            if (!IsAuthorized(userId, actionContext))
        //            {
        //                actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, $"Unauthorized access to resource! {actionContext.Request.RequestUri.AbsolutePath}");
        //                return;
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, $"Unauthorized access to resource! {actionContext.Request.RequestUri.AbsolutePath}");
        //        return;
        //    }

        //    base.OnAuthorization(actionContext);
        //}

        //private bool IsAuthorized(string userId, HttpActionContext actionContext)
        //{
        //    bool result = false;
        //    try
        //    {
        //        var requestScope = actionContext.Request.GetDependencyScope();

        //        var userBusinessRepository = requestScope.GetService(typeof(IUserBusinessRepository)) as IUserBusinessRepository;

        //        foreach (var role in allowedRoles)
        //        {
        //            result = userBusinessRepository.CanAccessResource(userId);
        //            if (result)
        //                break;
        //        }
                
        //    }
        //    catch (Exception)
        //    {
        //        result = false;
        //    }
        //    return result;
        //}
    }
}
