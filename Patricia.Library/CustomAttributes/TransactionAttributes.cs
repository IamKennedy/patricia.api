﻿using System;
using System.Transactions;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Patricia.Library.CustomAttributes
{
    public class TransactionAttributes : ActionFilterAttribute
    {
        [AttributeUsage(AttributeTargets.Method)]
        public class TransactionsAttribute : ActionFilterAttribute
        {
            private TransactionScope transactionScope;
            public override void OnActionExecuting(HttpActionContext actionContext)
            {
                var transactionOptions = new TransactionOptions
                {
                    IsolationLevel = IsolationLevel.ReadCommitted,
                    Timeout = new TimeSpan(0, 0, 10, 0, 0)
                };

                transactionScope = new TransactionScope(TransactionScopeOption.RequiresNew, transactionOptions);
            }
            public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
            {
                if (actionExecutedContext.Exception == null)
                {
                    transactionScope?.Complete();
                    transactionScope?.Dispose();
                }
                else
                {
                    transactionScope?.Dispose();
                }
            }
        }
    }
}
