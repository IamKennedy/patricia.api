﻿using Patricia.Library.Models;
using System.Collections.Generic;


namespace Patricia.Library.DataContracts
{
    public interface IUserDataRepository
    {
        User ValidateUser(string username, string password);
        User ValidateUserWithoutPassword(string username);
        User RetriveUserByUsername(string username);
        User RetriveUserByUserID(long userID);
        bool ChangePassword(long userId, string OldPassword, string newPassword);
        List<User> GetUsers();
        bool DisableUser(int userId);
        bool EnableUser(int userId);
        bool Save(User user);
        bool Exists(string username);
        bool Update(User user);
    }
}
