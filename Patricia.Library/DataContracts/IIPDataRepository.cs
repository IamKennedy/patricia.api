﻿using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.DataContracts
{
    public interface IIPDataRepository
    {
        bool Exists(string ip);
        List<IP> RetrieveIPS();
        IP RetrieveIPByID(long ipID);
        List<IP> RetrieveIPs();
        bool Save(IP ip);
        bool Update(IP ip);
    }
}
