﻿using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.DataContracts
{
    public interface IApprovalConfigDataRepository
    {
        List<ApprovalConfiguration> RetrieveAll();

        ApprovalConfiguration RetrieveByType(string type);

        bool Update(List<ApprovalConfiguration> configs);
    }
}
