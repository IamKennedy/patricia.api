﻿using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.DataContracts
{
    public interface IAuditTrailDataRepository
    {
        bool Save(AuditTrail auditTrail);

        List<AuditTrail> RetrieveAudits(string fromDate, string toDate);

    }
}
