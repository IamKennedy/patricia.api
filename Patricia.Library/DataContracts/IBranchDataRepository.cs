﻿using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.DataContracts
{
    public interface IBranchDataRepository
    {
        bool Exists(long id, string name, string code);

        List<Branch> RetrieveActiveBranches();

        bool Exists(string branch);

        Branch RetrieveBranchByID(long branchID);

        List<Branch> RetrieveBranches();

        bool Save(Branch branch);

        bool Update(Branch branch);


    }
}
