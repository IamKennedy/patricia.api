﻿using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.DataContracts
{
    public interface IApprovalDataRepository
    {
        List<Approval> RetrieveApprovals();

        bool UpdateApproval(Approval approval);

        List<Approval> RetrieveFilteredApprovals(string fromDate, string toDate, string status, string approvalType);

        Approval RetrieveByUsername(string username);

        Approval RetrieveById(long Id);

        bool Save(Approval approval);
    }
}
