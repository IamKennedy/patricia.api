﻿

using Patricia.Library.Models;
using System.Collections.Generic;

namespace Patricia.Library.DataContracts
{ 
    public interface ITransactionDataRepository
    {
        List<Transaction> RetrieveTransactions(string fromDateStr, string toDateStr);
        List<Transaction> RetrieveTransactionsByAccountNumber(string accountNumber);
        List<Transaction> RetrieveTransactionsByCardNumber(string hashedpan);

        Transaction RetrieveTransactionsByReferenceCode(string referenceID);
        bool Save(Transaction transaction);
    }
}
