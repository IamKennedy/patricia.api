﻿using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.DataContracts
{
    public interface ICardPRDataRepository
    {
        List<CardPrintRequest> RetrievePrintRequests();

        bool Save(CardPrintRequest printRequest);

        bool UpdatePrintRequest(CardPrintRequest newprintRequest, CardPrintRequest oldprintRequest);

        CardPrintRequest GetPrintRequestbyId(long requestId);

        List<CardPrintRequest> RetrieveFilteredRequests(string fromDate, string toDate, string status, string branch);
    }
}
