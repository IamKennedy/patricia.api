﻿

using Patricia.Library.Models;
using System.Collections.Generic;

namespace Patricia.Library.DataContracts
{
    public interface ICardDataRepository
    {
        Card GetCardByPan(string cardPan);
        Card GetCardByAccountNumber(string accountNumber);
        Card GetActiveCardByAccountNumber(string accountNumber);
        Card GetCardbyID(long id);
        List<Card> GetActiveCards();
        bool Block(string cardPan);
        bool Unblock(string cardPan);
        bool Save(Card card);
    }
}
