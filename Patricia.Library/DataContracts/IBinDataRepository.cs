﻿using Patricia.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.DataContracts
{
    public interface IBinDataRepository
    {
        bool Exists(string bin);

        List<Bin> RetrieveAll();

        Bin GetBinById(long Id);

        bool Update(Bin bin);

        bool Save(Bin bin);
    }
}
