﻿using Patricia.Library.CustomExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;

namespace Patrica.Library.Exceptions
{
    public class GlobalExceptionHandler : ExceptionHandler
    {
        public override Task HandleAsync(ExceptionHandlerContext context, CancellationToken cancellationToken)
        {
            HttpStatusCode statusCode = new HttpStatusCode();
            // STEP 1: exit if we cannot handle the exception (boilerplate code)

            // nothing we can do if the context is not present
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            // verify this exception should be handled at all; exit if not
            if (!ShouldHandle(context))
            {
                return Task.FromResult(0);
            }

            // STEP 2: Create an IHttpActionResult from the exception as required
            var ex = context.Exception;
            if (ex is InvalidException)
            {
                statusCode = HttpStatusCode.BadRequest;
            }
            else if (ex is NotFoundException)
            {
                statusCode = HttpStatusCode.NotFound;
            }
            else
            {
                statusCode = HttpStatusCode.InternalServerError;
            }

            var responseMsg = context.Request.CreateErrorResponse(statusCode, ex.Message);
            context.Result = new ResponseMessageResult(responseMsg);

            return Task.FromResult(0);
        }

        public override bool ShouldHandle(ExceptionHandlerContext context) => true;
    }
}