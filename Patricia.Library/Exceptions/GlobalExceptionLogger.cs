﻿using Patricia.Library.Common;
using Patricia.Library.CommonContracts;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.ExceptionHandling;

namespace Patrica.Library.Exceptions
{
    public class GlobalExceptionLogger : ExceptionLogger
    {
        /// <summary>
        /// Required ExceptionLogger method to process an exception
        /// </summary>
        /// <remarks>
        /// Important! Not every ExceptionLoggerContext field will be set depending on where
        /// the exception occurs, but you can minimally count on the Exception and Request properties.
        /// </remarks>
        public override Task LogAsync(ExceptionLoggerContext context, CancellationToken cancellationToken)
        {
            var ex = context.Exception;

            if (ex != null && ex.Message.Contains("Server cannot set status after HTTP headers have been sent")) return Task.FromResult(0);

            IErrorLogger errorLogger = new ErrorLogger();
            errorLogger.WriteError(ex, Enums.LogType.Error);

            return Task.FromResult(0);
        }
    }
}