﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using static System.String;

namespace Patricia.Library.Extensions
{
    public static class ExceptionExt
    {
        public static string ToLogString(this Exception exception, string environmentStackTrace)
        {
            var environmentStackTraceLines = GetUserStackTraceLines(environmentStackTrace);
            if (environmentStackTraceLines.Any())
            {
                environmentStackTraceLines.RemoveAt(0);
            }

            var stackTraceLines = GetStackTraceLines(exception.StackTrace);
            stackTraceLines.AddRange(environmentStackTraceLines);

            var fullStackTrace = Join(Environment.NewLine, stackTraceLines);

            var logMessage = exception.Message + Environment.NewLine + fullStackTrace;
            return logMessage;
        }

        /// <summary>
        ///  Gets a list of stack frame lines, as strings.
        /// </summary>
        /// <param name="stackTrace">Stack trace string.</param>
        private static List<string> GetStackTraceLines(string stackTrace)
        {
            return stackTrace != null ? stackTrace.Split(new[] { Environment.NewLine }, StringSplitOptions.None).ToList() : new List<string>();
        }

        /// <summary>
        ///  Gets a list of stack frame lines, as strings, only including those for which line number is known.
        /// </summary>
        /// <param name="fullStackTrace">Full stack trace, including external code.</param>
        private static List<string> GetUserStackTraceLines(string fullStackTrace)
        {
            var regex = new Regex(@"([^\)]*\)) in (.*):line (\d)*$");

            var stackTraceLines = GetStackTraceLines(fullStackTrace);

            return stackTraceLines.Where(stackTraceLine => regex.IsMatch(stackTraceLine)).ToList();
        }
    }
}
