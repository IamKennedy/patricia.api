﻿using System;
using System.Globalization;


namespace Patricia.Library.Extensions
{
    public static class DateExt
    {
        public static DateTime ToDate(this string date, string conversionFormat = "dd-MM-yyyy", bool addOneDay = false)
           => !string.IsNullOrEmpty(date) ? addOneDay ? DateTime.ParseExact(date, conversionFormat, CultureInfo.InvariantCulture).AddDays(1).Date : DateTime.ParseExact(date, conversionFormat, CultureInfo.InvariantCulture).Date : DateTime.Today.Date;
    }
}
