﻿using Patricia.Library.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.BusinessContracts
{
    public interface IIPBusinessRepository
    {
        List<IPDTO> RetrieveIPs();

        BaseDTO AddIP(IPDTO ip);

        BaseDTO UpdateIP(IPDTO ip);
        bool DoesIPExist(string ip);

    }
}
