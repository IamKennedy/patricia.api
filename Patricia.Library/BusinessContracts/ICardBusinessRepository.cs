﻿
using Patricia.Library.DTO;

namespace Patricia.Library.BusinessContracts
{
    public interface ICardBusinessRepository
    {
        BaseDTO BlockCard(PanDTO cardPan);
        BaseDTO UnBlockCard(PanDTO cardPan);
        CardDTO GetCardInfo(PanDTO cardPan);
        BaseDTO LinkCard(CardDTO card);
    }
}
