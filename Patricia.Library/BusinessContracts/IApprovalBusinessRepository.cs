﻿using Patricia.Library.DTO;
using System.Collections.Generic;
using static Patricia.Library.Common.Enums;

namespace Patricia.Library.BusinessContracts
{
    public interface IApprovalBusinessRepository
    {
        List<ApprovalDTO> RetrieveAll();

        List<ApprovalDTO> RetrieveFilteredApprovals(string fromDate, string toDate, string status, string approvalType);

        ApprovalDTO RetrieveByUserName(string username);

        BaseDTO Update(ApprovalDTO approval);

        bool IsTypeConfiguredForApproval(string type);
    }
}