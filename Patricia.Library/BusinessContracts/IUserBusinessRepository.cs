﻿using Patricia.Library.DTO;
using Patricia.Library.Models;
using System.Collections.Generic;


namespace Patricia.Library.BusinessContracts
{
    public interface IUserBusinessRepository
    {
        UsersDTO ValidateUser(UsersDTO _user);

        UsersDTO ValidateUsername(UsersDTO _user);

        UsersDTO RetrieveUserByUserName(UsersDTO _user);

        UsersDTO RetrieveUserByID(UsersDTO _user);

        List<UsersDTO> RetrieveUsers();

        BaseDTO ResetPassword(UsersDTO user);

        BaseDTO ForgotPassword(UsersDTO user);

        BaseDTO CreateUser(UsersDTO user);

        BaseDTO ChangePassword(PasswordDTO passwordDTO);

        BaseDTO UpdateUser(UsersDTO userDto);
    }
}
