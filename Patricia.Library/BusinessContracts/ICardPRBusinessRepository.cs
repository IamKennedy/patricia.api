﻿using Patricia.Library.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.BusinessContracts
{
    public interface ICardPRBusinessRepository
    {
        List<CardPRDTO> RetrieveFilteredPrintRequest(string fromDate, string toDate, string printStatus, string branchName);

        BaseDTO UpdateCardRequest(CardPRDTO printRequest, string username);

        BaseDTO ResetCardRequest(CardPRDTO printRequest);

        BaseDTO LogCardRequest(CardPRDTO printRequest);

        CardPRDTO CheckCardRequestStatus(long printrequestId);

    }
}
