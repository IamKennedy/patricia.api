﻿using Patricia.Library.DTO;
using System.Collections.Generic;


namespace Patricia.Library.BusinessContracts
{
    public interface IBranchBusinessRepository
    {
        List<BranchDTO> RetrieveBranches();

        BaseDTO AddBranch(BranchDTO branch);

        BaseDTO UpdateBranch(BranchDTO branch);

    }
}
