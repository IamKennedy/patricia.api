﻿using Patricia.Library.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patricia.Library.BusinessContracts
{
    public interface IBinBusinessRepository
    {
        List<BinDTO> RetrieveBins();

        BaseDTO AddBin(BinDTO bin);

        BaseDTO UpdateBin(BinDTO bin);


    }
}
