﻿

using Patricia.Library.DTO;
using System.Collections.Generic;

namespace Patricia.Library.BusinessContracts
{
    public interface IAuditTrailBusinessRespository
    {
        List<AuditDTO> RetrieveAll(string fromDate, string toDate);

    }
}
