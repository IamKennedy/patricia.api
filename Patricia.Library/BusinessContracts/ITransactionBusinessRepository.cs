﻿

using Patricia.Library.DTO;
using System.Collections.Generic;

namespace Patricia.Library.BusinessContracts
{
    public interface ITransactionBusinessRepository
    {
        List<TransactionsDTO> RetrieveTransactions(string fromDate, string toDate);
        List<TransactionsDTO> RetrieveTransactionsByAccountNumber(string accountNumber);
        List<TransactionsDTO> RetrieveTransactionsByCardNumber(string pan);
        bool SaveTransaction(TransactionsDTO transaction);
        TransactionsDTO RetrieveTransactionByReferenceID(string referenceID);
    }
}
