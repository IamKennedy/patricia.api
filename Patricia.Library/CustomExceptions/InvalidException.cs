﻿using System;


namespace Patricia.Library.CustomExceptions
{
    public class InvalidException : Exception
    {
        public InvalidException()
        {
        }

        public InvalidException(string message) : base(message)
        {
        }
    }
}
