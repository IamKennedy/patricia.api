﻿using Newtonsoft.Json;
using Patricia.Library.BusinessContracts;
using Patricia.Library.DataContracts;
using Patricia.Library.DTO;
using Swashbuckle.Swagger.Annotations;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Security.Claims;
using Newtonsoft.Json.Serialization;

namespace Patricia.API.Controllers
{
    [RoutePrefix("api/userapi")]
    public class UserAPIController : ApiController
    {
        private readonly IUserBusinessRepository userBusinessRepository;
        private readonly IApprovalBusinessRepository approvalBusinessRepository;
        private readonly IAuditTrailDataRepository auditTrailDataRepository;
        
        public UserAPIController(
           IUserBusinessRepository userBusinessRepository,
           IApprovalBusinessRepository approvalBusinessRepository,
           IAuditTrailDataRepository auditTrailDataRepository

         )
           
        {
            this.userBusinessRepository = userBusinessRepository;
            this.approvalBusinessRepository = approvalBusinessRepository;
            this.auditTrailDataRepository = auditTrailDataRepository;
            
        }

        [HttpPost, Route("AddUser")]
        [Authorize(Roles = "SUPER-ADMIN, ADMIN")]
        [SwaggerResponse(HttpStatusCode.Created, Type = typeof(BaseDTO))]
        public async Task<HttpResponseMessage> AddUser([FromBody]UsersDTO userDto)
        {
            return await Task.Run(() => {
                var result = new BaseDTO();
                userDto.overrideApproval = approvalBusinessRepository.IsTypeConfiguredForApproval("Create User");
                result = userBusinessRepository.CreateUser(userDto);            
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }

        [HttpPut, Route("UpdateUser")]
        [SwaggerResponse(HttpStatusCode.Created, Type = typeof(BaseDTO))]
        [Authorize(Roles = "SUPER-ADMIN, ADMIN")]
        public async Task<HttpResponseMessage> UpdateUser([FromBody]UsersDTO userDto)
        {
            return await Task.Run(() => {
                var result = new BaseDTO();
                userDto.overrideApproval = approvalBusinessRepository.IsTypeConfiguredForApproval("Update User");
                result = userBusinessRepository.UpdateUser(userDto);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
           
        }

        [HttpGet, Route("RetrieveUsers")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ListDTO<UsersDTO>))]
        [Authorize(Roles = "SUPER-ADMIN, ADMIN")]
        public async Task<HttpResponseMessage> RetrieveUsers()
        {
            return await Task.Run(() =>
            {
                var users = new ListDTO<UsersDTO>();
                users.data = userBusinessRepository.RetrieveUsers();
                users.responseCode = "00";
                users.responseMessage = "Users Retrieved Successfully";        
                var packedResponse = Request.CreateResponse(HttpStatusCode.OK, users);
                return packedResponse;
            });
           
        }

        
        [HttpPost, Route("AuthenticateUser")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(UsersDTO))]
        [Authorize(Roles = "SUPER-ADMIN, ADMIN, STANDARD-USER")]
        public async Task<HttpResponseMessage> AuthenticateUser([FromBody]UsersDTO userDto)
        {
            return await Task.Run(() => 
            {             
                var user = userBusinessRepository.ValidateUser(userDto);
                var packedResponse = Request.CreateResponse(HttpStatusCode.OK, user);
                return packedResponse;
            });
            
        }

        
        [HttpPost, Route("ResetPassword")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(BaseDTO))]
        [Authorize(Roles = "SUPER-ADMIN, ADMIN")]
        public async Task<HttpResponseMessage> ResetPassword([FromBody]UsersDTO userDto)
        {
            return await Task.Run(() =>
            {
                var user = userBusinessRepository.ForgotPassword(userDto);
                var packedResponse = Request.CreateResponse(HttpStatusCode.OK, user);
                return packedResponse;
            });

        }

        [HttpPost, Route("ChangePassword")]
        [Authorize(Roles = "SUPER-ADMIN, ADMIN, STANDARD-USER")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(BaseDTO))]
        public async Task<HttpResponseMessage> ChangePassword([FromBody]PasswordDTO passwordDto)
        {
            return await Task.Run(() =>
            {
                var user = userBusinessRepository.ChangePassword(passwordDto);
                var packedResponse = Request.CreateResponse(HttpStatusCode.OK, user);
                return packedResponse;
            });

        }




    }
}
