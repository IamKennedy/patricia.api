﻿
using Newtonsoft.Json;
using Patricia.Library.BusinessContracts;
using Patricia.Library.DTO;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Patricia.API.Controllers
{
    [RoutePrefix("api/IPAPI")]
    public class IPAPIController : ApiController
    {
        private readonly IIPBusinessRepository ipBusinessRepository;
        private readonly IApprovalBusinessRepository approvalBusinessRepository;

        
        public IPAPIController
        (
            IIPBusinessRepository ipBusinessRepository,
            IApprovalBusinessRepository approvalBusinessRepository
        )
        {
            this.approvalBusinessRepository = approvalBusinessRepository;
            this.ipBusinessRepository = ipBusinessRepository;
        }

        [HttpPost, Route("SaveIP")]
        [SwaggerResponse(HttpStatusCode.Created, Type = typeof(BaseDTO))]
        [Authorize(Roles = "SUPER-ADMIN, ADMIN")]
        public async Task<HttpResponseMessage> SaveIP([FromBody]IPDTO ipDTO)
        {
            return await Task.Run(() =>
            {
                var result = new BaseDTO();
                ipDTO.overrideApproval = approvalBusinessRepository.IsTypeConfiguredForApproval("Create IP");
                result = ipBusinessRepository.AddIP(ipDTO);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }

        [HttpPut, Route("UpdateIP")]
        [SwaggerResponse(HttpStatusCode.Created, Type = typeof(BaseDTO))]
        [Authorize(Roles = "SUPER-ADMIN, ADMIN")]
        public async Task<HttpResponseMessage> UpdateIP([FromBody]IPDTO ipDTO)
        {
            return await Task.Run(() =>
            {
                var result = new BaseDTO();
                ipDTO.overrideApproval = approvalBusinessRepository.IsTypeConfiguredForApproval("Update IP");
                result = ipBusinessRepository.UpdateIP(ipDTO);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }

        [HttpPost, Route("ValidateIP")]
        [SwaggerResponse(HttpStatusCode.Created, Type = typeof(BaseDTO))]
        [Authorize(Roles = "SUPER-ADMIN, ADMIN, STANDARD-USER")]
        public async Task<HttpResponseMessage> ValidateIP([FromBody]IPDTO ip)
        {
            return await Task.Run(() =>
            {
                var result = new BaseDTO();

                bool isIPValid  = ipBusinessRepository.DoesIPExist(ip.ipAddress);

                if (isIPValid)
                {
                    result.responseCode = "00";
                    result.responseMessage = "IP is Authorized and Valid";
                }
                else
                {
                    result.responseCode = "01";
                    result.responseMessage = "IP is not Authorized or Valid";
                }
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }

        [HttpGet, Route("RetrieveIPS")]
        [SwaggerResponse(HttpStatusCode.Created, Type = typeof(BaseDTO))]
        [Authorize(Roles = "SUPER-ADMIN, ADMIN")]
        public async Task<HttpResponseMessage> RetrieveIPS()
        {
            return await Task.Run(() =>
            {
                var result = new ListDTO<IPDTO>();
                result.data = ipBusinessRepository.RetrieveIPs();
                if (result != null)
                {
                    result.responseCode = "00";
                    result.responseMessage = "IPs Retrieved Successfully";
                }
                else
                {
                    result.responseCode = "01";
                    result.responseMessage = "No IP List";
                }
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
    }
}

