﻿using Newtonsoft.Json;
using Patricia.Library.BusinessContracts;
using Patricia.Library.DTO;
using Swashbuckle.Swagger.Annotations;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Patricia.API.Controllers
{
    public class BranchAPIController : ApiController
    {
        private readonly IBranchBusinessRepository branchBusinessRepository;
        private readonly IApprovalBusinessRepository approvalBusinessRepository;

        public BranchAPIController
        (
            IBranchBusinessRepository branchBusinessRepository,
            IApprovalBusinessRepository approvalBusinessRepository
        )
        {
            this.approvalBusinessRepository = approvalBusinessRepository;
            this.branchBusinessRepository = branchBusinessRepository;
        }

        [HttpPost, Route("SaveBranch")]
        [SwaggerResponse(HttpStatusCode.Created, Type = typeof(BaseDTO))]
        [Authorize(Roles = "ADMIN")]
        public async Task<HttpResponseMessage> SaveBranch([FromBody]BranchDTO branchDTO)
        {
            return await Task.Run(() =>
            {
                var result = new BaseDTO();
                branchDTO.overrideApproval = approvalBusinessRepository.IsTypeConfiguredForApproval("Create Branch");
                result = branchBusinessRepository.AddBranch(branchDTO);
                var response = JsonConvert.SerializeObject(result);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            });
        }

        [HttpPut, Route("UpdateBranch")]
        [SwaggerResponse(HttpStatusCode.Created, Type = typeof(BaseDTO))]
        [Authorize(Roles = "ADMIN")]
        public async Task<HttpResponseMessage> UpdateBranch([FromBody]BranchDTO branchDTO)
        {
            return await Task.Run(() =>
            {
                var result = new BaseDTO();
                branchDTO.overrideApproval = approvalBusinessRepository.IsTypeConfiguredForApproval("Update Branch");
                result = branchBusinessRepository.UpdateBranch(branchDTO);
                var response = JsonConvert.SerializeObject(result);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            });
        }

        [HttpGet, Route("RetrieveBranches")]
        [SwaggerResponse(HttpStatusCode.Created, Type = typeof(BaseDTO))]
        [Authorize(Roles = "ADMIN")]
        public async Task<HttpResponseMessage> RetrieveBranches()
        {
            return await Task.Run(() =>
            {
                var result = new ListDTO<BranchDTO>();
                result.data = branchBusinessRepository.RetrieveBranches();
                var response = JsonConvert.SerializeObject(result);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            });
        }
    }
}
