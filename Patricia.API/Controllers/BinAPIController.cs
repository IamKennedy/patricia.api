﻿using Newtonsoft.Json;
using Patricia.Library.BusinessContracts;
using Patricia.Library.DTO;
using Swashbuckle.Swagger.Annotations;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Patricia.API.Controllers
{
    public class BinAPIController : ApiController
    {
        private readonly IBinBusinessRepository binBusinessRepository;
        private readonly IApprovalBusinessRepository approvalBusinessRepository;

        public BinAPIController
        (
            IBinBusinessRepository binBusinessRepository,
            IApprovalBusinessRepository approvalBusinessRepository
        )
        {
            this.approvalBusinessRepository = approvalBusinessRepository;
            this.binBusinessRepository = binBusinessRepository;
        }


        [HttpPost, Route("SaveBin")]
        [SwaggerResponse(HttpStatusCode.Created, Type = typeof(BaseDTO))]
        [Authorize(Roles = "ADMIN")]
        public async Task<HttpResponseMessage> SaveBin([FromBody]BinDTO binDTO)
        {
            return await Task.Run(() =>
            {
                var result = new BaseDTO();
                binDTO.overrideApproval = approvalBusinessRepository.IsTypeConfiguredForApproval("Create Bin");
                result = binBusinessRepository.AddBin(binDTO);
                var response = JsonConvert.SerializeObject(result);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            });
        }

        [HttpPut, Route("UpdateBin")]
        [SwaggerResponse(HttpStatusCode.Created, Type = typeof(BaseDTO))]
        [Authorize(Roles = "ADMIN")]
        public async Task<HttpResponseMessage> UpdateBin([FromBody]BinDTO binDTO)
        {
            return await Task.Run(() =>
            {
                var result = new BaseDTO();
                binDTO.overrideApproval = approvalBusinessRepository.IsTypeConfiguredForApproval("Update Bin");
                result = binBusinessRepository.UpdateBin(binDTO);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }

        [HttpGet, Route("RetrieveBins")]
        [SwaggerResponse(HttpStatusCode.Created, Type = typeof(BaseDTO))]
        [Authorize(Roles = "ADMIN")]
        public async Task<HttpResponseMessage> RetrieveBins()
        {
            return await Task.Run(() =>
            {
                var result = new ListDTO<BinDTO>();
                result.data = binBusinessRepository.RetrieveBins();
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
    }
}
