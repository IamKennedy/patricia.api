﻿using Newtonsoft.Json;
using Patricia.Library.Attributes;
using Patricia.Library.BusinessContracts;
using Patricia.Library.DTO;
using Patricia.Library.Security;
using Swashbuckle.Swagger.Annotations;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using static Patricia.Library.CustomAttributes.TransactionAttributes;

namespace Patricia.API.Controllers
{
    [RoutePrefix("api/cardsapi")]    
    public class CardsAPIController : ApiController
    {
        private readonly ICardBusinessRepository cardBusinessRepository;
        private readonly IApprovalBusinessRepository approvalBusinessRepository;

        public CardsAPIController(
            ICardBusinessRepository cardBusinessRepository,
            IApprovalBusinessRepository approvalBusinessRepository)
        {
            this.cardBusinessRepository = cardBusinessRepository;
            this.approvalBusinessRepository = approvalBusinessRepository;
        }

        [HttpPost, Route("RetrieveCardInfo")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(BaseDTO))]
        [Authorize(Roles = "SUPER-ADMIN, ADMIN, STANDARD-USER")]
        public async Task<HttpResponseMessage> RetrieveCardInfo([FromBody]PanDTO card)
        {
            return await Task.Run(() => 
            {
                

                var cardInfo = cardBusinessRepository.GetCardInfo(card);
                if (cardInfo.accountNumber != null)
                {
                    cardInfo.responseCode = "00";
                    cardInfo.responseMessage = "Card Info Retrieved Successfully";
                }
                else
                {
                    cardInfo.responseCode = "01";
                    cardInfo.responseMessage = "Card Not Found";
                }
               
                var packedResponse = Request.CreateResponse(HttpStatusCode.OK, cardInfo);
                return packedResponse;
            });       
            
        }

        [HttpPost, Route("BlockCard")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(BaseDTO))]
        [Authorize(Roles = "SUPER-ADMIN, ADMIN, STANDARD-USER")]
        public async Task<HttpResponseMessage> BlockCard([FromBody]PanDTO cardNumber)
        {
            return await Task.Run(() =>
            {  
                cardNumber.isBlockCardConfiguredForApproval = approvalBusinessRepository.IsTypeConfiguredForApproval("Block Card");
                var card = cardBusinessRepository.BlockCard(cardNumber);           
                var packedResponse = Request.CreateResponse(HttpStatusCode.OK, card);
                return packedResponse;
            });
            
        }

        [HttpPost, Route("UnblockCard")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(BaseDTO))]
        [Authorize(Roles = "SUPER-ADMIN, ADMIN, STANDARD-USER")]
        public async Task<HttpResponseMessage> UnBlockCard([FromBody]PanDTO cardNumber)
        {
            return await Task.Run(() =>
            {
                cardNumber.isBlockCardConfiguredForApproval = approvalBusinessRepository.IsTypeConfiguredForApproval("Block Card");
                var card = cardBusinessRepository.UnBlockCard(cardNumber);
                var packedResponse = Request.CreateResponse(HttpStatusCode.OK, card);
                return packedResponse;
            });

        }

        [HttpPost, Route("LinkCard")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(BaseDTO))]
        [Authorize(Roles = "SUPER-ADMIN, ADMIN, STANDARD-USER")]
        public async Task<HttpResponseMessage> LinkCard([FromBody]CardDTO cardDTO)
        {
            return await Task.Run(() =>
            {
                BaseDTO response = new BaseDTO();           
                cardDTO.isLinkCardConfiguredForApproval = approvalBusinessRepository.IsTypeConfiguredForApproval("Link Card");
                var card = cardBusinessRepository.LinkCard(cardDTO);               
                var packedResponse = Request.CreateResponse(HttpStatusCode.OK, card);
                return packedResponse;
            });       
        }
    }
}
