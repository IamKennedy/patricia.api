﻿using Newtonsoft.Json;
using Patricia.Library.BusinessContracts;
using Patricia.Library.DTO;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Patricia.API.Controllers
{
    public class ApprovalAPIController : ApiController
    {
        private readonly IApprovalBusinessRepository approvalBusinessRepository;

        public ApprovalAPIController(IApprovalBusinessRepository approvalBusinessRepository)
        {
            this.approvalBusinessRepository = approvalBusinessRepository;
        }

        [HttpGet, Route("RetrieveApprovals/{fromDate}/{toDate}/{status}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ListDTO<ApprovalDTO>))]
        [Authorize(Roles = "ADMIN")]
        public async Task<HttpResponseMessage> RetrieveApprovals(string fromDate, string toDate, string status)
        {
            return await Task.Run(() => 
            {
                var approvals = new ListDTO<ApprovalDTO>();
                approvals.data = approvalBusinessRepository.RetrieveFilteredApprovals(fromDate, toDate, status, "");
                var responseJson = JsonConvert.SerializeObject(approvals);
                return Request.CreateResponse(HttpStatusCode.OK, responseJson);
            });          
        }

        [HttpPost, Route("DeclineApproval")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ListDTO<ApprovalDTO>))]
        [Authorize(Roles = "ADMIN")]
        public async Task<HttpResponseMessage> DeclineApproval(string fromDate, string toDate, string status)
        {
            return await Task.Run(() =>
            {
                var approvals = new ListDTO<ApprovalDTO>();
                approvals.data = approvalBusinessRepository.RetrieveFilteredApprovals(fromDate, toDate, status, "");
                var responseJson = JsonConvert.SerializeObject(approvals);
                return Request.CreateResponse(HttpStatusCode.OK, responseJson);
            });
        }
    }
}
