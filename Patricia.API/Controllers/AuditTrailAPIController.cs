﻿using Newtonsoft.Json;
using Patricia.Library.BusinessContracts;
using Patricia.Library.DTO;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Patricia.API.Controllers
{
    public class AuditTrailAPIController : ApiController
    {
        private readonly IAuditTrailBusinessRespository auditTrailBusinessRepository;

        public AuditTrailAPIController(IAuditTrailBusinessRespository auditTrailBusinessRepository)
        {
            this.auditTrailBusinessRepository = auditTrailBusinessRepository;
        }


        [HttpGet, Route("RetrieveAudits/{fromDate}/{toDate}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ListDTO<AuditDTO>))]
        [Authorize(Roles = "ADMIN")]
        public async Task<HttpResponseMessage> RetrieveAudits([FromUri]string fromDate, string toDate)
        {
            return await Task.Run(() =>
            {
                var audits = new ListDTO<AuditDTO>();
                audits.data = auditTrailBusinessRepository.RetrieveAll(fromDate, toDate);
                return Request.CreateResponse(HttpStatusCode.OK, audits);
            });
        }
    }
}
