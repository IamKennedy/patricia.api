﻿using Newtonsoft.Json;
using Patricia.Library.Attributes;
using Patricia.Library.BusinessContracts;
using Patricia.Library.DTO;
using Patricia.Library.Security;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using static Patricia.Library.CustomAttributes.TransactionAttributes;

namespace Patricia.API.Controllers
{
    [RoutePrefix("api/TransactionsAPI")]
    public class TransactionsAPIController : ApiController
    {
        private readonly ITransactionBusinessRepository transactionBusinessRepository;

        public TransactionsAPIController(
            ITransactionBusinessRepository transactionBusinessRepository)
        {
            this.transactionBusinessRepository = transactionBusinessRepository;
        }

        [HttpGet, Route("RetrieveTransactionsByAccountNumber/{AccountNumber}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ListDTO<TransactionsDTO>))]
        [Authorize(Roles = "STANDARD-USER")]
        public async Task<HttpResponseMessage> RetrieveTransactionsByAccountNumber([FromUri] string accountNumber)
        {
            return await Task.Run(() => 
            {
                var transactions = new ListDTO<TransactionsDTO>();
                transactions.data = transactionBusinessRepository.RetrieveTransactionsByAccountNumber(accountNumber);
                if (transactions != null)
                {
                    transactions.responseCode = "00";
                    transactions.responseMessage = "Transactions Retrieved Successfully";
                }
                else
                {
                    transactions.responseCode = "01";
                    transactions.responseMessage = "Transaction does not Exist";
                }

                var packedResponse = Request.CreateResponse(HttpStatusCode.OK, transactions);
                return packedResponse;
            });
           
        }

        [HttpGet, Route("RetrieveTransactions/{fromDate}/{toDate}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ListDTO<TransactionsDTO>))]
        [Authorize(Roles = "STANDARD-USER")]
        public async Task<HttpResponseMessage> RetrieveTransactions([FromUri] string fromDate, string toDate)
        {
            return await Task.Run(() =>
            {
                var transactions = new ListDTO<TransactionsDTO>();
                transactions.data = transactionBusinessRepository.RetrieveTransactions(fromDate, toDate);
                if (transactions != null)
                {
                    transactions.responseCode = "00";
                    transactions.responseMessage = "Transactions Retrieved Successfully";
                }
                else
                {
                    transactions.responseCode = "01";
                    transactions.responseMessage = "Transaction does not Exist";
                }

                
                var packedResponse = Request.CreateResponse(HttpStatusCode.OK, transactions);
                return packedResponse;
            });

        }

        [HttpPost, Route("SaveTransaction")]
        [SwaggerResponse(HttpStatusCode.Created, Type = typeof(BaseDTO))]
        [Authorize(Roles = "STANDARD-USER")]
        public async Task<HttpResponseMessage> SaveTransaction([FromBody]TransactionsDTO transactionDTO)
        {
            return await Task.Run(() =>
            {
                BaseDTO transactionResponse = new BaseDTO();


                var response = transactionBusinessRepository.SaveTransaction(transactionDTO);

                if (response)
                {
                    transactionResponse.responseCode = "00";
                    transactionResponse.responseMessage = "Transaction Saved Successfully";
                    return Request.CreateResponse(HttpStatusCode.OK, transactionResponse);
                }
                else
                {
                    transactionResponse.responseCode = "01";
                    transactionResponse.responseMessage = "Transaction Could Not Be Saved";
                    return Request.CreateResponse(HttpStatusCode.BadRequest, transactionResponse);
                }

            });
            
        }
    }
}
