﻿using Newtonsoft.Json;
using Patricia.Library.BusinessContracts;
using Patricia.Library.DTO;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Patricia.API.Controllers
{
    public class CardRequestAPIController : ApiController
    {
        private readonly ICardPRBusinessRepository cardRequestBusinessRepository;
        private readonly ICardBusinessRepository cardBusinessRepository;

        public CardRequestAPIController
            (
            ICardPRBusinessRepository cardRequestBusinessRepository, 
            ICardBusinessRepository cardBusinessRepository
            )
        {
            this.cardRequestBusinessRepository = cardRequestBusinessRepository;
            this.cardBusinessRepository = cardBusinessRepository;

        }

        [HttpPost, Route("LogCardRequest")]
        [SwaggerResponse(HttpStatusCode.Created, Type = typeof(BaseDTO))]
        [Authorize(Roles = "STANDARD-USER")]
        public async Task<HttpResponseMessage> LogCardRequest([FromBody]CardPRDTO cardRequestDto)
        {
            return await Task.Run(() =>
            {
                var result = new BaseDTO();
                result = cardRequestBusinessRepository.LogCardRequest(cardRequestDto);
                var response = JsonConvert.SerializeObject(result);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            });
        }

        [HttpPost, Route("ResetCardRequest")]
        [SwaggerResponse(HttpStatusCode.Created, Type = typeof(BaseDTO))]
        [Authorize(Roles = "ADMIN, STANDARD-USER")]
        public async Task<HttpResponseMessage> ResetCardRequest([FromBody]PanDTO cardRequestDto)
        {
            return await Task.Run(() =>
            {
                var result = new BaseDTO();
                var blockCard = cardBusinessRepository.BlockCard(cardRequestDto);
                return Request.CreateResponse(HttpStatusCode.OK, blockCard);
            });
        }

        [HttpGet, Route("RetrieveCardRequests")]
        [SwaggerResponse(HttpStatusCode.Created, Type = typeof(ListDTO<CardPRDTO>))]
        [Authorize(Roles = "ADMIN, STANDARD-USER")]
        public async Task<HttpResponseMessage> RetrieveCardRequests(string fromDate, string toDate, string status, string branch)
        {
            return await Task.Run(() =>
            {
                var cardRequests = new ListDTO<CardPRDTO>();
                cardRequests.data = cardRequestBusinessRepository.RetrieveFilteredPrintRequest(fromDate, toDate, status, branch);
                var results = JsonConvert.SerializeObject(cardRequests);
                return Request.CreateResponse(HttpStatusCode.OK, results);
            });
        }
    }
}
