﻿using System;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using System.Threading.Tasks;
using System.Security.Claims;
using Patricia.Library.Business;
using Patricia.Library.DTO;
using Patricia.Library.Data;
using Patricia.Library.Common;
using Patricia.Library.Models;
using Microsoft.Owin.Security;
using System.Collections.Generic;
using Microsoft.Owin.Security.Infrastructure;

[assembly: OwinStartup(typeof(Patricia.API.App_Start.Startup))]


namespace Patricia.API.App_Start
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            var myAuthProvider = new MyAuthProvider();

            OAuthAuthorizationServerOptions options = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,

                //The Path For generating the Toekn
                TokenEndpointPath = new PathString("/tokenManager"),

                //Setting the Token Expired Time (5 minutes)
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(5),

                //myAuthProvider class will validate the user credentials
                Provider = myAuthProvider,

                //For creating the refresh token and regenerate the new access token
                RefreshTokenProvider = new RefreshTokenProvider()
            };

            app.UseOAuthAuthorizationServer(options);

            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());


            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);

        
        }
    }

    public class MyAuthProvider : OAuthAuthorizationServerProvider
    {

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            await Task.Run(() =>
            {
                string clientId = string.Empty;
                string clientSecret = string.Empty;

                // The TryGetBasicCredentials method checks the Authorization header and
                // Return the ClientId and clientSecret
                if (!context.TryGetBasicCredentials(out clientId, out clientSecret))
                {
                    context.SetError("invalid_client", "Client credentials could not be retrieved through the Authorization header.");

                    return Task.FromResult<object>(null);
                }

                // Check the existence of the client by calling the ValidateClient method
                var client = (new ClientDataRepository()).ValidateClient(clientId, clientSecret);

                if (client == null)
                {
                    // Client could not be validated.
                    context.SetError("invalid_client", "Client credentials are invalid.");

                    return Task.FromResult<object>(null);
                }
                else
                {
                    if (!client.Active)
                    {
                        context.SetError("invalid_client", "Client is inactive.");
                        return Task.FromResult<object>(null);
                    }
                    // Client has been verified.
                    context.OwinContext.Set<PatriciaClient>("ta:client", client);

                    context.OwinContext.Set<string>("ta:clientAllowedOrigin", client.AllowedOrigin);

                    context.OwinContext.Set<string>("ta:clientRefreshTokenLifeTime", client.RefreshTokenLifeTime.ToString());

                    context.Validated();

                    return Task.FromResult<object>(null);
                }
            });

        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            await Task.Run(() =>
            {
                PatriciaClient client = context.OwinContext.Get<PatriciaClient>("ta:client");

                var allowedOrigin = context.OwinContext.Get<string>("ta:clientAllowedOrigin");

                if (allowedOrigin == null)
                {
                    allowedOrigin = "*";
                }
                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

                UsersDTO userdto = new UsersDTO
                {
                    username = context.UserName,

                    password = context.Password
                };

                var userCredentials = UserDataRepository.ValidateToken(userdto.username, CryptoLib.SHA512(userdto.password));

                var identity = new ClaimsIdentity(context.Options.AuthenticationType);

                if (userCredentials != null)
                {
                    identity.AddClaim(new Claim(ClaimTypes.Name, userCredentials.Username));

                    identity.AddClaim(new Claim(ClaimTypes.Email, userCredentials.Email));

                    identity.AddClaim(new Claim(ClaimTypes.Role, userCredentials.Role));

                    context.Validated(identity);

                    var props = new AuthenticationProperties(new Dictionary<string, string> {

                        {
                            "client_id", (context.ClientId == null) ? string.Empty : context.ClientId
                        },

                        {
                            "userName", context.UserName
                        }
                    });

                    var ticket = new AuthenticationTicket(identity, props);

                    context.Validated(ticket);
                }

            });

        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }
            return Task.FromResult<object>(null);
        }

        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            var originalClient = context.Ticket.Properties.Dictionary["client_id"];

            var currentClient = context.ClientId;

            if (originalClient != currentClient)
            {
                context.SetError("invalid_clientId", "Refresh token is issued to a different clientId.");
                return Task.FromResult<object>(null);
            }

            // Change auth ticket for refresh token requests
            var newIdentity = new ClaimsIdentity(context.Ticket.Identity);

            newIdentity.AddClaim(new Claim("newClaim", "newValue"));

            var newTicket = new AuthenticationTicket(newIdentity, context.Ticket.Properties);

            context.Validated(newTicket);

            return Task.FromResult<object>(null);
        }

    }

    public class RefreshTokenProvider : IAuthenticationTokenProvider
    {
        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            //Get the client ID from the Ticket properties
            var clientid = context.Ticket.Properties.Dictionary["client_id"];

            if (string.IsNullOrEmpty(clientid))
            {
                return;
            }
            //Generating a Uniqure Refresh Token ID
            var refreshTokenId = Guid.NewGuid().ToString("n");

            AuthenticationDataRepository _repo = new AuthenticationDataRepository();
            
            // Getting the Refesh Token Life Time From the Owin Context
            var refreshTokenLifeTime = context.OwinContext.Get<string>("ta:clientRefreshTokenLifeTime");

            //Creating the Refresh Token object
            var token = new RefreshToken()
            {
                //storing the RefreshTokenId in hash format
                ID = CryptoLib.GetHash(refreshTokenId),
                ClientID = clientid,
                UserName = context.Ticket.Identity.Name,
                IssuedTime = DateTime.UtcNow,
                ExpiredTime = DateTime.UtcNow.AddMinutes(Convert.ToDouble(refreshTokenLifeTime))
            };
            //Setting the Issued and Expired time of the Refresh Token
            context.Ticket.Properties.IssuedUtc = token.IssuedTime;

            context.Ticket.Properties.ExpiresUtc = token.ExpiredTime;

            token.ProtectedTicket = context.SerializeTicket();

            var result = await _repo.AddRefreshToken(token);

            if (result)
            {
                context.SetToken(refreshTokenId);
            }
            
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            var allowedOrigin = context.OwinContext.Get<string>("ta:clientAllowedOrigin");

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            string hashedTokenId = CryptoLib.GetHash(context.Token);

            AuthenticationDataRepository _repo = new AuthenticationDataRepository();
            
                var refreshToken = await _repo.FindRefreshToken(hashedTokenId);

                if (refreshToken != null)
                {
                    //Get protectedTicket from refreshToken class
                    context.DeserializeTicket(refreshToken.ProtectedTicket);
                    var result = await _repo.RemoveRefreshTokenByID(hashedTokenId);
                }
            
        }

        public void Create(AuthenticationTokenCreateContext context)
        {
            throw new NotImplementedException();
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            throw new NotImplementedException();
        }

    }
}
