﻿using Patricia.Library.Common;
using Patricia.Library.CommonContracts;
using Patricia.Library.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace Patricia.API
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AutoFacLoader.Initialize(GlobalConfiguration.Configuration, Assembly.GetExecutingAssembly());
            GlobalConfiguration.Configuration.IncludeErrorDetailPolicy =
                IncludeErrorDetailPolicy.Default;
            GlobalConfiguration.Configure(WebApiConfig.Register);
            //AreaRegistration.RegisterAllAreas();
            //RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_BeginRequest()
        {
            HttpContext.Current.Response.AddHeader("X-Frame-Options", "DENY");
            if (Request.Headers.AllKeys.Contains("Origin") && Request.HttpMethod == "OPTIONS")
            {
                Response.Flush();
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            IErrorLogger errorLogger = new ErrorLogger();
            errorLogger.WriteError(ex, Enums.LogType.Error);
        }
    }
}
